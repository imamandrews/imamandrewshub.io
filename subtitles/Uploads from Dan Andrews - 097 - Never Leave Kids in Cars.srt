1
00:00:03,080 --> 00:00:04,389
we're going to get some lunch I need you

2
00:00:05,370 --> 00:00:08,009
to stay here I'll be very quick okay

3
00:00:07,469 --> 00:00:08,070
I'm gonna be very quick how many times

4
00:00:13,380 --> 00:00:16,949
have you opted for the easy option like

5
00:00:15,539 --> 00:00:19,378
leaving the kids in the car while you

6
00:00:16,949 --> 00:00:20,489
quickly run into the shops and I bet you

7
00:00:19,379 --> 00:00:22,499
never thought you're actually putting

8
00:00:20,489 --> 00:00:24,687
your kids lives in danger by doing so

9
00:00:22,500 --> 00:00:26,639
within minutes the temperature inside a

10
00:00:24,689 --> 00:00:29,429
parked car can be twice as hot as the

11
00:00:26,640 --> 00:00:31,409
outside my boys body temperature rises

12
00:00:29,428 --> 00:00:33,929
three to five times faster than mine and

13
00:00:31,410 --> 00:00:37,589
guess what leaving the window down has

14
00:00:33,929 --> 00:00:39,238
very little effect on the inside okay

15
00:00:37,590 --> 00:00:41,939
boys I need you to be quick we've got to

16
00:00:39,238 --> 00:00:43,648
go into the shops leaving your kids in

17
00:00:41,939 --> 00:00:46,288
the car is dangerous at any time of the

18
00:00:43,649 --> 00:00:49,078
year but it can be catastrophic over

19
00:00:46,289 --> 00:00:51,559
summer our valuables should never be

20
00:00:49,079 --> 00:00:56,109
left in the car

21
00:00:51,560 --> 00:00:58,170
[Music]

22
00:00:56,109 --> 00:00:58,169
you

