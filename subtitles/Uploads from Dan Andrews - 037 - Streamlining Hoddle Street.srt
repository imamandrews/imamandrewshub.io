1
00:00:01,719 --> 00:00:06,689
The first stage of the streamlining Hoddle
Street initiative will make your journey along

2
00:00:06,689 --> 00:00:13,330
and across Hoddle Street safer and more reliable.
This corridor is a critical link to the Eastern,

3
00:00:13,330 --> 00:00:18,970
Monash, Citylink, and West Gate Freeways,
and a connection between the City and the inner suburbs.

4
00:00:18,970 --> 00:00:26,880
Each day 330,000 people travel
along or across Hoddle Street.

5
00:00:26,880 --> 00:00:33,590
At the Swan Street intersection, large numbers of vehicles, trams, buses, cyclists, and pedestrians compete for space on the road.

6
00:00:33,590 --> 00:00:39,730
That is why it has been
chosen as the site to pilot this new concept: a Continuous Flow Intersection.

7
00:00:39,730 --> 00:00:47,100
Continuous Flow Intersections allow more people to travel

8
00:00:47,100 --> 00:00:54,890
through a green light by changing when and
where right turns are made. This means better traffic flow for all road users.

9
00:01:09,000 --> 00:01:12,180
The new Swan
Street intersection will transform your journey.

