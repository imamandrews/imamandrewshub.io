1
00:00:01,430 --> 00:00:06,130
Premier Daniel Andrews has landed in Seattle
to begin a 9-day tour of America. He used

2
00:00:06,130 --> 00:00:11,090
a visit to Microsoft to unveil a partnership
designed to inspire more Victorian students

3
00:00:11,090 --> 00:00:16,690
into the booming IT sector. State political
reporter Andrew Lund is in the United States.

4
00:00:20,440 --> 00:00:24,620
Dodging flying fish became the latest hazard
of Victoria's top job.

5
00:00:25,580 --> 00:00:27,580
One more! One more!

6
00:00:27,580 --> 00:00:32,579
As the Premier touched down in Seattle, a
city with football on the front page and its

7
00:00:32,579 --> 00:00:34,339
own version of skyrail.

8
00:00:34,339 --> 00:00:39,619
You'll see elevated rail in all the great cities of the world and that's why Melbourne needs it also.

9
00:00:39,620 --> 00:00:43,200
Seattle is also home to tech giant Microsoft and the

10
00:00:43,200 --> 00:00:47,420
sprawling headquarters was the first official
stop for Daniel Andrews.

11
00:00:47,420 --> 00:00:52,720
You can imagine customers in a retailer using this stuff to try on clothes virtually.

12
00:00:52,720 --> 00:00:58,460
The Premier met junior high students learning coding, seen by many as the language of the future.

13
00:00:59,140 --> 00:01:06,800
It's not exactly Java, but it's not exactly C++, so it's kind of like Scratch.

14
00:01:06,800 --> 00:01:09,180
He announced plans for an Australian-first

15
00:01:09,180 --> 00:01:15,110
coding contest, giving Victorian students
in years 6, 8 and 10 the opportunity to pitch

16
00:01:15,110 --> 00:01:20,190
ideas for apps or games in the hope of inspiring
the next Bill Gates.

17
00:01:20,190 --> 00:01:27,240
I think the chances are pretty ding dang good actually. We've seen lots of great ideas coming from young people.

18
00:01:27,250 --> 00:01:31,780
The Premier is due to meet the Microsoft president
later in his trip, which will take him from

19
00:01:31,780 --> 00:01:36,100
Seattle to New York, Boston, San Francisco and Los Angeles.

20
00:01:36,100 --> 00:01:39,700
It's going to be a very important trip with a lot of positive announcements.

21
00:01:39,940 --> 00:01:42,840
I'm getting younger! There you go. Very good.

22
00:01:42,840 --> 00:01:45,640
In Seattle, Andrew Lund, Nine News.

