1
00:00:01,209 --> 00:00:06,220
The Western Distributor will provide a vital
second river crossing while saving 20 minutes

2
00:00:06,220 --> 00:00:09,049
when travelling from the west.

3
00:00:09,049 --> 00:00:14,170
Ongoing design work and feedback from communities,
councils and industry has been incorporated

4
00:00:14,170 --> 00:00:16,789
into the project's reference design.

5
00:00:16,789 --> 00:00:20,909
Congestion and traffic flow along the West
Gate Freeway will be improved by widening

6
00:00:20,909 --> 00:00:27,189
from 8 to 12 lanes. A new road layout including
express lanes to the West Gate bridge will

7
00:00:27,189 --> 00:00:30,749
improve reliability and reduce travel times.

8
00:00:30,749 --> 00:00:39,300
A connection a Hyde St moves trucks directly
between local industry and the freeway.

9
00:00:39,300 --> 00:00:44,170
The design includes a seamless entry and exit
to the Western Distributor tunnel with portals

10
00:00:44,170 --> 00:00:50,989
within the West Gate freeway, taking trucks
off local roads in the inner west and will

11
00:00:50,989 --> 00:00:56,640
provide direct access to Australia's busiest
container port meaning more efficient movement

12
00:00:56,640 --> 00:01:01,829
of goods. The project's urban design strategy
responds to feedback and will enhance the

13
00:01:01,829 --> 00:01:06,909
places through which it passes. An elevated
section along Footscray Road will provide

14
00:01:06,909 --> 00:01:12,090
access to East Swanston Dock, City Link and
City North, taking through traffic out of

15
00:01:12,090 --> 00:01:18,780
the CBD and supporting new job precincts.
And the project will now include nearly 10kms

16
00:01:18,780 --> 00:01:21,950
of new and upgraded cycling and walking paths.

17
00:01:21,950 --> 00:01:27,189
There will be more opportunities to have your
say about this design and innovative solutions

18
00:01:27,189 --> 00:01:30,880
by tenderers for the construction of Melbourne's
Western Distributor.

