1
00:00:00,860 --> 00:00:06,480
The Victorian Government is delivering the $440 million Murray Basin Rail Project.

2
00:00:07,720 --> 00:00:10,120
Standardising the Murray Basin's rail network

3
00:00:10,120 --> 00:00:14,000
provides industry with better access to the national rail network,

4
00:00:14,000 --> 00:00:16,720
enabling movement of goods such as grain,

5
00:00:16,720 --> 00:00:18,140
mineral sands,

6
00:00:18,140 --> 00:00:22,400
and fresh produce to neighbouring states and to international markets.

7
00:00:22,400 --> 00:00:24,960
The Murray Basin Rail Project will allow

8
00:00:24,960 --> 00:00:27,600
an extra 500,000 tonnes of grain,

9
00:00:27,600 --> 00:00:30,580
and 450,000 tonnes of general freight,

10
00:00:30,580 --> 00:00:32,500
including wine, food and nuts,

11
00:00:32,500 --> 00:00:35,060
to be transported by rail per year.

12
00:00:35,060 --> 00:00:39,380
It will remove an estimated 20,000 truck trips from our roads every year,

13
00:00:39,380 --> 00:00:42,100
create almost 300 construction jobs,

14
00:00:42,100 --> 00:00:44,820
and provide regional economic opportunities,

15
00:00:44,820 --> 00:00:47,580
including the production of concrete sleepers, which will

16
00:00:47,580 --> 00:00:50,860
support more than 20 jobs at Austrak in Geelong.

17
00:00:52,020 --> 00:00:57,360
To find out more, visit ptv.vic.gov.au.

