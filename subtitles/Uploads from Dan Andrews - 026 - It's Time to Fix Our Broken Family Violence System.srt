1
00:00:00,269 --> 00:00:05,839
Last year, 37 Victorians were murdered by
the people they trusted the most.

2
00:00:05,839 --> 00:00:09,400
And we failed every single one of them.

3
00:00:09,400 --> 00:00:14,510
Family violence is a silent crime, deployed
by cowards behind closed doors.

4
00:00:14,510 --> 00:00:19,830
It’s Australia’s number one law-and-order
issue – but it’s taken us too long to

5
00:00:19,830 --> 00:00:23,620
admit it.
The numbers are staggering; the human cost

6
00:00:23,620 --> 00:00:27,360
unacceptable.
And one thing is for certain: more of the

7
00:00:27,360 --> 00:00:31,850
same policies will only mean more of the same
tragedies.

8
00:00:31,850 --> 00:00:35,730
Well, now it’s time to change it all.

9
00:00:35,730 --> 00:00:40,600
Today, Australia’s first Royal Commission
into Family Violence hands down its full and

10
00:00:40,600 --> 00:00:44,840
final report.
This Commission received one thousand submissions.

11
00:00:44,840 --> 00:00:50,350
It has given us thousands of pages of
answers – and a raft of recommendations

12
00:00:50,350 --> 00:00:57,370
that will save Australian lives.
We will implement every single one of them.

13
00:00:57,370 --> 00:01:01,750
We will overhaul our broken support system
from the bottom up.

14
00:01:01,750 --> 00:01:06,899
We will punish the perpetrators of this violence,
we will listen to the people who survived

15
00:01:06,899 --> 00:01:12,039
it, and we will change the culture that created
it.

16
00:01:12,039 --> 00:01:15,560
In doing so, Victoria won’t just be leading
the nation.

17
00:01:15,560 --> 00:01:18,939
We’ll be leading the world.

18
00:01:18,939 --> 00:01:22,840
We have one chance, here.
And I refuse to look back in ten years’

19
00:01:22,840 --> 00:01:28,549
time and admit that we could have done more
to save innocent lives.

20
00:01:28,549 --> 00:01:33,829
The fact is we’ve failed enough already.
We’re going to get this right.

21
00:01:33,829 --> 00:01:38,630
And we’re going to do it by listening to
the survivors, working with the professionals

22
00:01:38,630 --> 00:01:51,419
who support them, and bringing the Victorian
people along with us, every step of the way.

