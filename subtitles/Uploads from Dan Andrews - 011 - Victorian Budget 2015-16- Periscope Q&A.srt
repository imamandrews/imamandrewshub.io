1
00:00:00,000 --> 00:00:03,209
hi I'm dad andrew is the premier of

2
00:00:01,709 --> 00:00:04,471
Victorian leader of the Labour Party

3
00:00:03,209 --> 00:00:04,559
this is our first periscope press

4
00:00:06,179 --> 00:00:10,347
conference it's a press conference

5
00:00:07,769 --> 00:00:12,299
without any journalists so you can see

6
00:00:10,349 --> 00:00:14,518
why we might do a lot of these in coming

7
00:00:12,300 --> 00:00:15,598
weeks and months no no it's always got

8
00:00:14,519 --> 00:00:18,090
to talk to journalists but it's even

9
00:00:15,599 --> 00:00:20,579
better sometimes to talk to Victorians

10
00:00:18,089 --> 00:00:23,968
about obviously the budget but also

11
00:00:20,579 --> 00:00:25,287
their take on things and there's lots of

12
00:00:23,969 --> 00:00:28,438
questions already come through just a

13
00:00:25,289 --> 00:00:33,570
bit of housekeeping I'd ask people to

14
00:00:28,439 --> 00:00:34,919
twin 22 hashtag ask the premier and the

15
00:00:33,570 --> 00:00:36,119
be from time to time some things that

16
00:00:34,920 --> 00:00:37,920
are perhaps can't give you all the

17
00:00:36,119 --> 00:00:43,529
detail to take too long to do it but if

18
00:00:37,920 --> 00:00:45,059
you go to premier geoau you'll be able

19
00:00:43,530 --> 00:00:47,190
to follow the links to the budget this

20
00:00:45,058 --> 00:00:49,319
year and be able to look at all the

21
00:00:47,189 --> 00:00:50,758
different information that's there about

22
00:00:49,320 --> 00:00:53,669
the investments were making now I don't

23
00:00:50,759 --> 00:00:55,138
want to talk for too long on any one

24
00:00:53,670 --> 00:00:57,509
particular subject we want to keep this

25
00:00:55,140 --> 00:00:59,939
moving as fast as we can I'm told this

26
00:00:57,509 --> 00:01:01,919
is something that is a first for

27
00:00:59,939 --> 00:01:05,639
certainly Victorian politics and maybe

28
00:01:01,920 --> 00:01:07,530
more broadly I hope to vow to do this on

29
00:01:05,640 --> 00:01:09,450
a fairly regular basis it's great to be

30
00:01:07,530 --> 00:01:11,369
able to answer real questions from real

31
00:01:09,450 --> 00:01:12,959
people and get outside the kind of

32
00:01:11,368 --> 00:01:15,688
Spring Street bubble it went all too

33
00:01:12,959 --> 00:01:17,008
often caught up in just quickly this is

34
00:01:15,688 --> 00:01:18,658
a budget that delivers sound financial

35
00:01:17,009 --> 00:01:20,069
management that's really important if

36
00:01:18,659 --> 00:01:22,079
you don't manage the state's finances

37
00:01:20,069 --> 00:01:24,269
well you don't have the money to be able

38
00:01:22,079 --> 00:01:26,309
to invest in the things that really

39
00:01:24,269 --> 00:01:29,007
matter so there's a surplus this year

40
00:01:26,310 --> 00:01:31,109
and each year over the next four and

41
00:01:29,009 --> 00:01:33,209
then there's some investments that I'm

42
00:01:31,109 --> 00:01:35,368
very very proud of in fact all of my

43
00:01:33,209 --> 00:01:37,859
team are really proud to have made

44
00:01:35,368 --> 00:01:40,108
decisions like almost four billion

45
00:01:37,859 --> 00:01:43,227
dollars for our schools our kinders and

46
00:01:40,109 --> 00:01:45,208
take a big boost to health really

47
00:01:43,228 --> 00:01:47,188
significant boost in public transport in

48
00:01:45,209 --> 00:01:50,098
fact the biggest investment in public

49
00:01:47,188 --> 00:01:51,898
transport we've ever seen and then lots

50
00:01:50,099 --> 00:01:54,389
of other commitments and not just in

51
00:01:51,899 --> 00:01:55,319
Melbourne right across Victoria will

52
00:01:54,390 --> 00:01:58,110
probably cover a little bit of that

53
00:01:55,319 --> 00:02:01,258
ground as people send in their Twitter

54
00:01:58,109 --> 00:02:02,968
questions let's get started that's

55
00:02:01,259 --> 00:02:06,299
probably the best thing to do so the

56
00:02:02,968 --> 00:02:10,078
first question comes from Jenny Ashby

57
00:02:06,299 --> 00:02:12,779
and she asks when will our 196 students

58
00:02:10,080 --> 00:02:15,330
know if we're getting a new school

59
00:02:12,780 --> 00:02:17,579
Epsom primary now for those of you who

60
00:02:15,330 --> 00:02:19,410
aren't from Epsom episodes a suburb of

61
00:02:17,580 --> 00:02:20,879
Bendigo basically deterrence you've just

62
00:02:19,409 --> 00:02:22,317
on the edge of Bendigo and it's part of

63
00:02:20,879 --> 00:02:25,138
a really big growth corridor at the

64
00:02:22,318 --> 00:02:26,939
moment we were up myself and jacinta

65
00:02:25,139 --> 00:02:28,529
allan who's the local member and other

66
00:02:26,939 --> 00:02:30,479
members of the team we're up there last

67
00:02:28,530 --> 00:02:32,999
year making commitments to Epsom primary

68
00:02:30,479 --> 00:02:35,578
skorts great teachers great kids great

69
00:02:33,000 --> 00:02:37,738
parent community but the school's a bit

70
00:02:35,580 --> 00:02:39,870
rundown in fact that's worse than that

71
00:02:37,739 --> 00:02:41,130
it's it needs to be replaced we're

72
00:02:39,870 --> 00:02:43,919
committed to do that and I'm really

73
00:02:41,129 --> 00:02:46,079
pleased to tell Jenny Ashby who must be

74
00:02:43,919 --> 00:02:47,428
one of the parents from Epsom or at

75
00:02:46,080 --> 00:02:51,839
least she's got a close connection to

76
00:02:47,430 --> 00:02:53,760
the school that the full more than five

77
00:02:51,840 --> 00:02:57,030
million dollars i think 5.6 million

78
00:02:53,759 --> 00:02:58,469
dollars was in today's budget and we'll

79
00:02:57,030 --> 00:03:00,840
be able to begin now the planning

80
00:02:58,469 --> 00:03:02,879
process so the construction can begin as

81
00:03:00,840 --> 00:03:05,759
soon as possible and we can get those

82
00:03:02,879 --> 00:03:07,499
kids are brand-new school for a brand

83
00:03:05,759 --> 00:03:09,658
new start I think that's really

84
00:03:07,500 --> 00:03:11,488
important hopefully there'll be lots of

85
00:03:09,659 --> 00:03:14,189
questions about schools and education

86
00:03:11,489 --> 00:03:15,750
kinders and TAFE throughout this evening

87
00:03:14,189 --> 00:03:17,639
because education really is a

88
00:03:15,750 --> 00:03:19,109
significant part of this budget as i

89
00:03:17,639 --> 00:03:21,808
said before nearly four billion dollars

90
00:03:19,110 --> 00:03:23,459
and jenny if you want your further

91
00:03:21,810 --> 00:03:26,399
information you can certainly visit a

92
00:03:23,459 --> 00:03:29,489
premier cup today you and there'll be

93
00:03:26,400 --> 00:03:31,650
some more details but this is more than

94
00:03:29,489 --> 00:03:34,200
five million for epson primary delivered

95
00:03:31,650 --> 00:03:38,249
in full in the budget today Oh next one

96
00:03:34,199 --> 00:03:39,958
we've got Amy Feldman only Feltman asked

97
00:03:38,250 --> 00:03:43,889
me as you can see the budget is titled

98
00:03:39,959 --> 00:03:46,199
for families amy has asked me she's keen

99
00:03:43,889 --> 00:03:49,139
to know if singles double income no kids

100
00:03:46,199 --> 00:03:52,139
or the elderly are of any interest to

101
00:03:49,139 --> 00:03:53,908
the four families government I don't

102
00:03:52,139 --> 00:03:56,429
know a bit more positive on these things

103
00:03:53,909 --> 00:03:58,827
that it seems you are families are

104
00:03:56,430 --> 00:04:00,269
that's basically all of us we're all

105
00:03:58,829 --> 00:04:03,739
part of a family were all brothers and

106
00:04:00,269 --> 00:04:06,238
sisters were all sons daughters partners

107
00:04:03,739 --> 00:04:07,529
some of us a single and repair and some

108
00:04:06,239 --> 00:04:09,328
of us are married we're all part of a

109
00:04:07,530 --> 00:04:12,299
family in fact the Victorian community

110
00:04:09,329 --> 00:04:13,920
is in many respects of family this is a

111
00:04:12,299 --> 00:04:17,039
budget that invests for everybody and

112
00:04:13,919 --> 00:04:20,159
let me just go through schools hospitals

113
00:04:17,038 --> 00:04:22,108
a safer community taking real action on

114
00:04:20,160 --> 00:04:24,450
family family violence investing in

115
00:04:22,108 --> 00:04:26,669
better public transport investing in

116
00:04:24,449 --> 00:04:28,348
better roads I think their role

117
00:04:26,670 --> 00:04:30,630
to every single Victorian I think that's

118
00:04:28,350 --> 00:04:32,189
the definition of fairness and Families

119
00:04:30,629 --> 00:04:34,559
a better offer because of it but I mean

120
00:04:32,189 --> 00:04:35,818
I don't know I've got a different view

121
00:04:34,560 --> 00:04:39,180
of these things I suppose but thanks

122
00:04:35,819 --> 00:04:43,198
very much for your tweet to moving on

123
00:04:39,180 --> 00:04:45,090
now to TAFE David David tweets how many

124
00:04:43,199 --> 00:04:47,788
years will it take the government to

125
00:04:45,089 --> 00:04:50,279
repair the damage to TAFE inflicted by

126
00:04:47,790 --> 00:04:52,589
the Liberals I want to thank David for

127
00:04:50,279 --> 00:04:55,759
his question and I would remind him to

128
00:04:52,589 --> 00:04:57,539
renew his AARP membership very very soon

129
00:04:55,759 --> 00:04:59,999
David's obviously a pretty strong

130
00:04:57,540 --> 00:05:02,160
supporter look I think it will take some

131
00:05:00,000 --> 00:05:04,620
time it was really significant damage

132
00:05:02,160 --> 00:05:06,270
but I'm very proud to have made a

133
00:05:04,620 --> 00:05:09,179
promise before the election to spend

134
00:05:06,269 --> 00:05:12,269
more money in TAFE to stop the cuts and

135
00:05:09,180 --> 00:05:14,1000
to stop that the real you know taupe

136
00:05:12,269 --> 00:05:18,688
taupe was in a terrible mess it's not

137
00:05:15,000 --> 00:05:20,099
fixed yet but we've kind of stabilized

138
00:05:18,689 --> 00:05:22,468
these things a little bit there's more

139
00:05:20,100 --> 00:05:24,030
than 300 million dollars indeed 350

140
00:05:22,470 --> 00:05:26,280
million dollars in the budget delivered

141
00:05:24,029 --> 00:05:28,408
today Steve who is doing a fantastic job

142
00:05:26,279 --> 00:05:30,448
in this area is visited every tape

143
00:05:28,410 --> 00:05:32,850
Institute across the state this money

144
00:05:30,449 --> 00:05:34,228
will be shared throughout Victoria and I

145
00:05:32,850 --> 00:05:36,030
just think that's fair i think that's

146
00:05:34,230 --> 00:05:38,190
what a good government does don't cut

147
00:05:36,029 --> 00:05:41,128
TAFE and make it harder for particularly

148
00:05:38,189 --> 00:05:42,658
young people particularly those in the

149
00:05:41,129 --> 00:05:45,539
outer suburbs and in regional Victoria

150
00:05:42,660 --> 00:05:47,759
to get the skills that they need for the

151
00:05:45,540 --> 00:05:50,099
job that they want it's a simple thing

152
00:05:47,759 --> 00:05:51,749
but if you don't invest in vocational

153
00:05:50,100 --> 00:05:54,300
education and training then you

154
00:05:51,750 --> 00:05:56,129
compromise people's future and every

155
00:05:54,300 --> 00:05:58,439
Victorian that's held back or that holds

156
00:05:56,129 --> 00:06:00,629
us all back so big investment today

157
00:05:58,439 --> 00:06:02,068
there'll be more next year and the year

158
00:06:00,629 --> 00:06:04,589
after that and the year after that

159
00:06:02,069 --> 00:06:06,628
because we value pay for me now how

160
00:06:04,589 --> 00:06:08,459
important it is how long it will take to

161
00:06:06,629 --> 00:06:09,928
repair the damage I suppose only bit

162
00:06:08,459 --> 00:06:11,789
more positive than that David let's not

163
00:06:09,930 --> 00:06:14,460
think about it in terms of damage let's

164
00:06:11,790 --> 00:06:16,230
make type better than it's ever been and

165
00:06:14,459 --> 00:06:22,378
today we took a very big step towards

166
00:06:16,230 --> 00:06:24,569
that next Mario Grande this is a

167
00:06:22,379 --> 00:06:27,029
charming why would you invest in murder

168
00:06:24,569 --> 00:06:28,708
there's a bit more but that's lots of

169
00:06:27,029 --> 00:06:30,629
four little i won't i won't go into that

170
00:06:28,709 --> 00:06:35,308
but we made a commitment about moon

171
00:06:30,629 --> 00:06:38,188
derail we took the line 22 south south

172
00:06:35,310 --> 00:06:38,879
south morang and we will take it to

173
00:06:38,189 --> 00:06:41,068
mandir

174
00:06:38,879 --> 00:06:42,989
this is a significant investments one of

175
00:06:41,069 --> 00:06:44,819
the biggest suburban rail upgrades that

176
00:06:42,990 --> 00:06:46,650
the state's ever seen you've got to get

177
00:06:44,819 --> 00:06:48,779
the planning right we've made that

178
00:06:46,649 --> 00:06:50,879
decision today nine million dollars in

179
00:06:48,779 --> 00:06:54,208
additional money to plan out the moon to

180
00:06:50,879 --> 00:06:56,398
rail link and what's more from a murder

181
00:06:54,209 --> 00:06:58,978
point of view I visit there quite often

182
00:06:56,399 --> 00:07:01,318
and it's a great local community but it

183
00:06:58,980 --> 00:07:03,510
can be better through more schools and

184
00:07:01,319 --> 00:07:05,098
we're delivering those better public

185
00:07:03,509 --> 00:07:07,648
transport better roads we're delivering

186
00:07:05,100 --> 00:07:09,420
those also and of course taking the

187
00:07:07,649 --> 00:07:12,028
pressure off health services in our

188
00:07:09,420 --> 00:07:14,700
growing corridors a growth corridors is

189
00:07:12,029 --> 00:07:16,109
very important as well you're not a big

190
00:07:14,699 --> 00:07:17,699
moon der fan but this government

191
00:07:16,110 --> 00:07:19,889
certainly is and Daniel Green is our

192
00:07:17,699 --> 00:07:21,538
local members are not only a great

193
00:07:19,889 --> 00:07:23,728
champion for her electorate but she

194
00:07:21,540 --> 00:07:24,810
lives there she's passionate and she

195
00:07:23,730 --> 00:07:26,700
makes sure i understand just how

196
00:07:24,810 --> 00:07:29,550
important things are for murder and for

197
00:07:26,699 --> 00:07:32,968
that whole growing north and i'm moving

198
00:07:29,550 --> 00:07:36,059
on will Ross will Ross asks what were

199
00:07:32,970 --> 00:07:39,359
your new LGBTI commissioners say

200
00:07:36,060 --> 00:07:42,050
regarding the budgets zero dollars for

201
00:07:39,360 --> 00:07:44,459
expanding overwhelmed gender dysphoria

202
00:07:42,050 --> 00:07:47,269
services at the Royal Children's

203
00:07:44,459 --> 00:07:49,728
Hospital in Melbourne well I don't think

204
00:07:47,269 --> 00:07:51,898
we'll that will be a matter that the

205
00:07:49,730 --> 00:07:53,730
LGBTI Commissioner will need to turn

206
00:07:51,899 --> 00:07:55,709
their mind to because there is more

207
00:07:53,730 --> 00:07:57,899
money for these gender dysphoria

208
00:07:55,709 --> 00:08:00,598
services at the Royal Children's

209
00:07:57,899 --> 00:08:02,698
Hospital I have had the great pleasure

210
00:08:00,600 --> 00:08:04,229
of being very closely connected with the

211
00:08:02,699 --> 00:08:06,899
new Royal Children's Hospital for many

212
00:08:04,230 --> 00:08:08,999
years now they do great work not only

213
00:08:06,899 --> 00:08:11,038
will this service this important clinic

214
00:08:09,000 --> 00:08:12,419
which is a statewide service be as I

215
00:08:11,040 --> 00:08:14,939
understand it the recipient of

216
00:08:12,420 --> 00:08:16,199
additional funds but what's more the

217
00:08:14,939 --> 00:08:19,408
Royal Children's Hospital will have a

218
00:08:16,199 --> 00:08:20,939
much stronger budget because you can

219
00:08:19,410 --> 00:08:23,400
have the best building you can have the

220
00:08:20,939 --> 00:08:24,448
best staff and they are amazing stuff at

221
00:08:23,399 --> 00:08:26,519
the Royal Children's Hospital I know

222
00:08:24,449 --> 00:08:29,368
that it's a politician and in our it as

223
00:08:26,519 --> 00:08:30,868
a parent too but unless they've got a

224
00:08:29,370 --> 00:08:33,419
strong budget they just can't do the

225
00:08:30,870 --> 00:08:35,669
work that they're trying to do so will

226
00:08:33,418 --> 00:08:37,199
there is extra money there for that

227
00:08:35,668 --> 00:08:38,548
service and for the Royal Children's

228
00:08:37,200 --> 00:08:40,740
Hospital more broadly and I hope that's

229
00:08:38,549 --> 00:08:46,468
a comfort to you and of everybody who's

230
00:08:40,740 --> 00:08:49,679
appearing across our state next I've got

231
00:08:46,470 --> 00:08:51,359
one here from so Tony Morton has asked

232
00:08:49,679 --> 00:08:52,409
me does the new six-lane Chandler

233
00:08:51,360 --> 00:08:55,919
highway breach include

234
00:08:52,409 --> 00:08:57,447
full time segregated bus lines I'm not

235
00:08:55,919 --> 00:09:00,268
sure about that only I think that part

236
00:08:57,448 --> 00:09:01,619
of today's decision is being is that

237
00:09:00,269 --> 00:09:03,419
more than a hundred million dollars to

238
00:09:01,620 --> 00:09:05,550
get this project away is to do the

239
00:09:03,419 --> 00:09:08,218
detailed planning to do the design to

240
00:09:05,549 --> 00:09:09,299
talk to the local community I think it's

241
00:09:08,220 --> 00:09:11,369
probably going to finish up being a

242
00:09:09,299 --> 00:09:13,109
brand new bridge built quite close to

243
00:09:11,370 --> 00:09:14,338
the current one and the current one then

244
00:09:13,110 --> 00:09:17,159
can be used for all sorts of other

245
00:09:14,339 --> 00:09:18,989
things whether it be bicycles

246
00:09:17,159 --> 00:09:21,897
pedestrians as I've seen some ideas

247
00:09:18,990 --> 00:09:24,360
about perhaps a kind of a public open

248
00:09:21,899 --> 00:09:26,339
space for for markets and things there's

249
00:09:24,360 --> 00:09:27,779
lots of different things you can do

250
00:09:26,339 --> 00:09:29,100
their most important thing to

251
00:09:27,778 --> 00:09:32,638
acknowledge is we're getting on with it

252
00:09:29,100 --> 00:09:34,470
I'm sure that Fiona Richardson or make

253
00:09:32,639 --> 00:09:36,860
sure we deliver a good outcome she's

254
00:09:34,470 --> 00:09:38,878
been a passionate advocate on this issue

255
00:09:36,860 --> 00:09:40,828
the money's there in this year's budget

256
00:09:38,879 --> 00:09:42,028
planning consultation listening to the

257
00:09:40,828 --> 00:09:46,198
local community will be a really

258
00:09:42,028 --> 00:09:55,979
important part of it now I've got one

259
00:09:46,198 --> 00:09:58,469
here from Caroline fields who says who

260
00:09:55,980 --> 00:10:00,989
asks me do you think the Real Housewives

261
00:09:58,470 --> 00:10:04,529
of Melbourne is a good or a bad thing

262
00:10:00,990 --> 00:10:06,560
for the image of Melbourne i've not

263
00:10:04,528 --> 00:10:09,539
watched the show so I don't know whether

264
00:10:06,559 --> 00:10:11,249
it is or it isn't I might leave others

265
00:10:09,539 --> 00:10:13,707
feel more expert yourself included

266
00:10:11,250 --> 00:10:16,078
Caroline to make that judgment what I

267
00:10:13,708 --> 00:10:17,908
would say the image of s States really

268
00:10:16,078 --> 00:10:20,337
important and the way we project to our

269
00:10:17,909 --> 00:10:22,378
region to other parts of Australia and

270
00:10:20,339 --> 00:10:23,519
to the world is that's very important

271
00:10:22,379 --> 00:10:25,499
because it's ultimately about whether

272
00:10:23,519 --> 00:10:27,479
people are in work the visitor economy

273
00:10:25,500 --> 00:10:30,509
is really important really important

274
00:10:27,480 --> 00:10:33,539
this tourism and major events and all of

275
00:10:30,509 --> 00:10:35,459
that sector if you're like that's 20

276
00:10:33,539 --> 00:10:37,138
billion dollars a year and 200,000

277
00:10:35,458 --> 00:10:40,169
Victorians whose livelihoods depend on

278
00:10:37,139 --> 00:10:42,329
it this budget Toydarian dollars for a

279
00:10:40,169 --> 00:10:43,378
brand new additional convention center

280
00:10:42,328 --> 00:10:45,359
space so we can get all the big

281
00:10:43,379 --> 00:10:46,919
conferences to come to our city those

282
00:10:45,360 --> 00:10:49,230
delegates can spend their money for

283
00:10:46,919 --> 00:10:50,669
hotels full bars for restaurants that's

284
00:10:49,230 --> 00:10:52,230
a really important thing and then

285
00:10:50,669 --> 00:10:55,318
there's 80 million dollar so we can have

286
00:10:52,230 --> 00:10:57,449
a new and improved series of major

287
00:10:55,318 --> 00:10:59,218
events lots of one-off events whether

288
00:10:57,448 --> 00:11:01,778
it's college football baseball

289
00:10:59,220 --> 00:11:03,338
basketball all of those sorts of things

290
00:11:01,778 --> 00:11:06,638
and are really strong offering down at

291
00:11:03,339 --> 00:11:08,499
the National Gallery error image is

292
00:11:06,639 --> 00:11:09,969
important and I think that today's

293
00:11:08,499 --> 00:11:11,829
budget is from a tourism and major

294
00:11:09,970 --> 00:11:15,370
events point of view has really

295
00:11:11,828 --> 00:11:17,198
delivered significantly to that that has

296
00:11:15,370 --> 00:11:18,758
nothing to do with the Real Housewives

297
00:11:17,198 --> 00:11:21,669
of Melbourne and I'm sorry I can't do

298
00:11:18,759 --> 00:11:24,220
any better than that on and I think we

299
00:11:21,669 --> 00:11:27,568
might be getting close to time I've got

300
00:11:24,220 --> 00:11:29,708
one here from James which of your many

301
00:11:27,568 --> 00:11:32,377
infrastructure projects will be started

302
00:11:29,708 --> 00:11:35,198
during this term not planned but

303
00:11:32,379 --> 00:11:38,018
actually started James pre best if we

304
00:11:35,198 --> 00:11:39,337
get you a full list but some projects we

305
00:11:38,019 --> 00:11:40,688
make no apology they're going to take

306
00:11:39,339 --> 00:11:42,519
more than four years to do it melbourne

307
00:11:40,688 --> 00:11:47,468
metro will not be delivered in for four

308
00:11:42,519 --> 00:11:50,919
years nine kilometres attract extras 25

309
00:11:47,470 --> 00:11:52,540
new underground stations a public

310
00:11:50,919 --> 00:11:54,338
transport system where you can turn up

311
00:11:52,539 --> 00:11:56,888
and you don't need a timetable you can

312
00:11:54,339 --> 00:11:58,898
just get the next train and go that's

313
00:11:56,889 --> 00:12:00,069
not for you that's not a four-year

314
00:11:58,899 --> 00:12:02,229
project that's going to take much longer

315
00:12:00,068 --> 00:12:04,028
than that but 1.5 billion dollars in

316
00:12:02,230 --> 00:12:05,888
this year's budget we will have

317
00:12:04,028 --> 00:12:07,539
delivered at least 20 of those level

318
00:12:05,889 --> 00:12:09,610
crossings they'll be gone we hope to do

319
00:12:07,539 --> 00:12:11,647
better if we can 20s the promise that we

320
00:12:09,610 --> 00:12:13,600
made over four years and then there's a

321
00:12:11,649 --> 00:12:17,439
whole range of hospitals and schools and

322
00:12:13,600 --> 00:12:19,359
all sorts of other investments that will

323
00:12:17,438 --> 00:12:23,888
start from this budget and we'll be

324
00:12:19,360 --> 00:12:25,388
finished by the end of 2018 at least we

325
00:12:23,889 --> 00:12:27,789
probably can give you a list at some

326
00:12:25,389 --> 00:12:30,220
point if you go to if you had tweet me

327
00:12:27,789 --> 00:12:32,798
your details and we can maybe send you

328
00:12:30,220 --> 00:12:34,809
something by email or you know you two

329
00:12:32,799 --> 00:12:37,598
can go to premier dr. Gupta I you and

330
00:12:34,808 --> 00:12:39,518
follow the budget links if you send us

331
00:12:37,600 --> 00:12:41,819
your details of maybe it's better if i

332
00:12:39,519 --> 00:12:45,990
write to you with some of those answers

333
00:12:41,818 --> 00:12:49,568
now I've got I think time for one more

334
00:12:45,990 --> 00:12:52,389
unless is I have a lot of time for one

335
00:12:49,568 --> 00:12:53,978
more Nick Wishart how many goals will

336
00:12:52,389 --> 00:12:55,869
Archie Thompson score this friday night

337
00:12:53,980 --> 00:12:59,529
in the Melbourne Derby a league final I

338
00:12:55,870 --> 00:13:02,828
don't know how many he'll score let's

339
00:12:59,528 --> 00:13:04,028
say let's hope he scores three but this

340
00:13:02,828 --> 00:13:05,497
is going to be fantastic I caught up

341
00:13:04,028 --> 00:13:09,298
with some of the guys from melbourne

342
00:13:05,499 --> 00:13:12,399
victory just last week and I was with

343
00:13:09,299 --> 00:13:15,018
Melbourne City a few weeks before that

344
00:13:12,399 --> 00:13:17,719
we can be really really proud that the

345
00:13:15,019 --> 00:13:19,488
all game is so popular the beautiful

346
00:13:17,720 --> 00:13:21,050
game is such a big part of our sporting

347
00:13:19,490 --> 00:13:23,420
culture and I know my youngest son

348
00:13:21,049 --> 00:13:26,329
Joseph who loves soccer started off

349
00:13:23,419 --> 00:13:28,129
playing auskick now he got soccer you

350
00:13:26,330 --> 00:13:31,190
can see just how important that is in

351
00:13:28,129 --> 00:13:32,779
his little life and I families right

352
00:13:31,190 --> 00:13:34,430
across the suburbs and regional Victoria

353
00:13:32,779 --> 00:13:37,338
a touched by it and we've got such an

354
00:13:34,429 --> 00:13:40,488
elite offering the best stadiums the

355
00:13:37,340 --> 00:13:43,609
best teams it's a great game and just a

356
00:13:40,490 --> 00:13:45,740
final plug on football the world game

357
00:13:43,610 --> 00:13:48,499
football the international champions cup

358
00:13:45,740 --> 00:13:50,589
starting in the middle of the year this

359
00:13:48,500 --> 00:13:54,200
is going to be fantastic manchester city

360
00:13:50,590 --> 00:13:56,059
real madrid and AS Roma this is going to

361
00:13:54,200 --> 00:13:57,530
be blockbuster football the best of the

362
00:13:56,059 --> 00:14:00,769
beautiful game in the world's most

363
00:13:57,529 --> 00:14:03,408
livable city and sported capital Nick I

364
00:14:00,769 --> 00:14:06,259
hope you there I really very much do so

365
00:14:03,409 --> 00:14:09,498
I think this has been our first I i

366
00:14:06,259 --> 00:14:11,089
assume won't be our last periscope press

367
00:14:09,500 --> 00:14:14,049
conference a press conference with no

368
00:14:11,090 --> 00:14:17,349
journalists that's that's a good thing

369
00:14:14,049 --> 00:14:19,818
I'm sure I've done my best Ron Burgundy

370
00:14:17,350 --> 00:14:22,010
impersonation possible it's great to

371
00:14:19,820 --> 00:14:23,900
interact with people real people asking

372
00:14:22,009 --> 00:14:25,518
real questions about the budget than

373
00:14:23,899 --> 00:14:27,918
what it means for them thanks for your

374
00:14:25,519 --> 00:14:29,239
feedback thanks for being part of this

375
00:14:27,919 --> 00:14:31,448
and we look forward to seeing you again

376
00:14:29,240 --> 00:14:31,449
soon

