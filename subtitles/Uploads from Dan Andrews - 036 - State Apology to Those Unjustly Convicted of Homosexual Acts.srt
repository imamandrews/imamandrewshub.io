1
00:00:00,669 --> 00:00:06,470
These laws did not just punish homosexual
acts, they punished homosexual thought. They

2
00:00:06,470 --> 00:00:13,469
had no place in a liberal democracy. They
have no place anywhere. The Victorian Parliament

3
00:00:13,469 --> 00:00:18,610
and the Victorian Government were at fault.
For this, we are sorry. On behalf of this

4
00:00:18,610 --> 00:00:25,890
house, we express our deepest regret. Speaker
– it’s never too late to put things right.

5
00:00:25,890 --> 00:00:29,960
It’s never too late to say sorry – and
mean it.

6
00:00:29,960 --> 00:00:35,749
That’s what brings us all to the heart of
our democracy today here in this Parliament where

7
00:00:35,749 --> 00:00:41,359
over the course of decades, a powerful prejudice
was written into law.

8
00:00:41,359 --> 00:00:44,049
A prejudice that ruined lives.

9
00:00:44,049 --> 00:00:49,690
A prejudice that prevails in different ways,
even still.

10
00:00:49,690 --> 00:00:55,519
That law was written in our name – as representatives,
and as Victorians.

11
00:00:55,519 --> 00:01:02,899
And that law was enforced by the very democratic
system to which we call ourselves faithful.

12
00:01:02,899 --> 00:01:08,680
So it is our responsibility to prove that
the Parliament that engineered this prejudice

13
00:01:08,680 --> 00:01:13,439
can also be the Parliament that ends it.

14
00:01:13,439 --> 00:01:18,229
That starts with acknowledging the offences
of the past admitting the failings of the

15
00:01:18,229 --> 00:01:27,100
present and building a society for the future
that is strong and fair and just.

16
00:01:27,100 --> 00:01:33,799
In doing so, Speaker, we’ll have shown this
moment to be no mere gesture.

17
00:01:33,799 --> 00:01:40,009
In doing so, we’ll have proven that the
dignity and bravery of generations of Victorians

18
00:01:40,009 --> 00:01:43,979
wasn’t simply for nought.

19
00:01:43,979 --> 00:01:49,079
And that, I hope, will be the greatest comfort
of all.

20
00:01:49,079 --> 00:01:53,270
Speaker, there is no more simple an acknowledgement
than this:

21
00:01:53,270 --> 00:01:59,539
There was a time in our history when we turned
thousands of ordinary young men into criminals.

22
00:01:59,539 --> 00:02:04,829
And it was profoundly and unimaginably wrong.

23
00:02:04,829 --> 00:02:10,030
That such a thing could have occurred – once,
perhaps a century ago – would not surprise

24
00:02:10,030 --> 00:02:11,780
most Victorians.

25
00:02:11,780 --> 00:02:19,599
Well, Speaker, I hold here an article that reports
the random arrest of 15 men.

26
00:02:19,599 --> 00:02:25,819
“Police Blitz Catches Homosexuals”, that’s what the
headline reads.

27
00:02:25,819 --> 00:02:33,619
And said a police officer: “…we just seem
to find homosexuals loitering wherever we go.”

28
00:02:33,629 --> 00:02:41,269
This was published in Melbourne’s biggest-selling
weekly newspaper – not 100 years ago, but in 1976.

29
00:02:41,269 --> 00:02:47,760
A decade earlier, in 1967, a local paper said
that a dozen men would soon face court for

30
00:02:47,760 --> 00:02:53,980
“morals offences”, and urged
the public to report homosexuals to the police

31
00:02:53,980 --> 00:02:57,460
with a minimum of delay.

32
00:02:57,460 --> 00:03:05,260
A generation earlier, in 1937, Judge MacIndoe
said John, a man in his 20s, was “not quite

33
00:03:05,260 --> 00:03:11,260
sane”, and gaoled him for three months on
a charge of gross indecency.

34
00:03:11,260 --> 00:03:17,470
In 1936, Jack, a working man from Sale, faced
a Melbourne court on the same charge – and

35
00:03:17,470 --> 00:03:20,340
he was gaoled for ten years.

36
00:03:20,340 --> 00:03:26,099
This, Speaker, is the society that we built.

37
00:03:26,099 --> 00:03:32,000
And it would be easy to blame the courts,
or the media, or the police, or the public even.

38
00:03:32,000 --> 00:03:35,939
It is easy for us to condemn their bigotry.

39
00:03:35,939 --> 00:03:41,000
But the law required them to be bigoted.

40
00:03:41,000 --> 00:03:47,439
And those laws were struck here, where I stand.

41
00:03:47,440 --> 00:03:55,840
One of those laws even earned the label abominable, not something we use very often in our statute books.

42
00:03:55,840 --> 00:04:00,580
In 1961 alone, 40 Victorian men were charged
with it.

43
00:04:00,590 --> 00:04:05,920
In the same year, a minor offence was created
that shook just as many lives.

44
00:04:05,920 --> 00:04:10,829
The penalty was $600 in today’s terms, or
one month imprisonment.

45
00:04:10,829 --> 00:04:15,510
The charge? ‘Loitering for homosexual purposes’.

46
00:04:15,510 --> 00:04:22,980
This was the offence used to justify that
random police blitz in 1976.

47
00:04:22,980 --> 00:04:29,220
A witness said: “Young policemen were sent…to…entrap suspected homosexuals.”

48
00:04:29,220 --> 00:04:33,850
“[Officers] dressed in swimwear…engaging
other men in conversation.”

49
00:04:33,850 --> 00:04:40,290
“When the policeman was satisfied the person
was homosexual, an arrest was made.”

50
00:04:40,290 --> 00:04:45,970
When we began this process, Speaker, I expected
to be offering an apology to people persecuted

51
00:04:45,970 --> 00:04:48,890
for homosexual acts.

52
00:04:48,890 --> 00:04:55,510
But it has become clear to me that the State
also persecuted against homosexual thought.

53
00:04:55,510 --> 00:04:59,650
Loitering for homosexual purposes is a thought
crime.

54
00:04:59,650 --> 00:05:05,370
And in one summer in 1976, in one location
alone, one hundred men were targeted under

55
00:05:05,370 --> 00:05:13,660
this violation of thought; something for which
there was no possible defence.

56
00:05:13,660 --> 00:05:18,320
All in our lifetimes, Speaker. All in our lifetimes.

57
00:05:18,320 --> 00:05:21,820
And what’s more in our name.

58
00:05:21,820 --> 00:05:26,230
Young people. Older people. Thousands and thousands
of people.

59
00:05:26,230 --> 00:05:31,270
I suppose it’s rare when you can’t even
begin to conceive what was on the minds of

60
00:05:31,270 --> 00:05:34,070
our forebears in this Place.

61
00:05:34,070 --> 00:05:39,040
But I look back at those statutes and I am
dumbfounded.

62
00:05:39,040 --> 00:05:47,270
I can’t possibly explain why we made these
laws, and clung to them, and fought for them.

63
00:05:47,270 --> 00:05:52,440
For decades, we were obsessed with the private
mysteries of men.

64
00:05:52,440 --> 00:05:54,100
And so we jailed them.

65
00:05:54,100 --> 00:05:59,400
We harmed them. And, in turn, they harmed themselves.

66
00:05:59,410 --> 00:06:05,160
Speaker, it is the first responsibility of
a Government to keep people safe.

67
00:06:05,160 --> 00:06:08,840
But the Government didn’t keep LGBTI people
safe.

68
00:06:08,840 --> 00:06:15,160
The Government invalidated their humanity
and cast them into a nightmare.

69
00:06:15,160 --> 00:06:21,110
And those who live today are the survivors
of nothing less than a campaign of destruction,

70
00:06:21,110 --> 00:06:24,400
led by the might of this State.

71
00:06:24,400 --> 00:06:30,100
Speaker, I had the privilege of meeting with four of
those survivors recently.

72
00:06:30,100 --> 00:06:32,250
One of them was Noel Tovey.

73
00:06:32,250 --> 00:06:35,560
He was sent to Pentridge in 1951.

74
00:06:35,560 --> 00:06:39,720
On more than one occasion in jail, he planned
his suicide.

75
00:06:39,720 --> 00:06:46,960
“Max was singing an aria from La Traviata
when the police arrived,” he recalled in his book

76
00:06:46,970 --> 00:06:52,790
I was very naive. I knew having sex with men
was against the law but I didn’t understand

77
00:06:52,790 --> 00:06:55,230
why it was a crime.

78
00:06:55,230 --> 00:06:59,600
At the hearing, the judge said, “You have
been charged with the abominable crime of

79
00:06:59,600 --> 00:07:02,580
buggery. How do you plead?”

80
00:07:02,580 --> 00:07:05,910
The maximum sentence was fifteen years.

81
00:07:05,910 --> 00:07:12,780
Afterwards, only two people would talk to
me. I couldn’t…get a job. I was a known

82
00:07:12,780 --> 00:07:16,060
criminal. And it’s ironic.

83
00:07:16,100 --> 00:07:23,860
Eventually I would have been forgiven by everyone
if I had murdered Max, but no one could forgive

84
00:07:23,860 --> 00:07:26,690
me for having sex with him.”

85
00:07:26,690 --> 00:07:34,650
And Noel, in his own words, quite amazingly, in a sign of strength, calls himself “one of the lucky ones.”

86
00:07:34,650 --> 00:07:37,060
I also met Terry Kennedy.

87
00:07:37,060 --> 00:07:40,470
He was 18 years old when he was arrested.

88
00:07:40,470 --> 00:07:45,410
“When I wanted to go overseas”, Terry
told me, “and when I wanted to start my

89
00:07:45,410 --> 00:07:48,840
own business, there was always that dreaded
question:

90
00:07:48,840 --> 00:07:53,440
“Have you ever been convicted of a criminal
offence?”

91
00:07:53,440 --> 00:07:55,410
I lied, of course.

92
00:07:55,410 --> 00:08:01,080
Then the phone rang…It was an inspector
from the St Kilda Police Station. He’d found

93
00:08:01,080 --> 00:08:03,110
me out.

94
00:08:03,110 --> 00:08:08,340
With a curse like that always lurking over
our heads, we always had to ask ourselves

95
00:08:08,340 --> 00:08:14,730
[this question] – just how far can I go
today?”

96
00:08:14,730 --> 00:08:20,330
That’s the sort of question which, in some
form or another, must have been asked by almost

97
00:08:20,330 --> 00:08:24,650
every single LGBTI person.

98
00:08:24,650 --> 00:08:31,260
And it is still asked today – by teenagers in
the schoolyard; by adults in the family home.

99
00:08:31,260 --> 00:08:37,490
Yes, the law was unjust, but it is wrong to
think its only victims were those who faced

100
00:08:37,490 --> 00:08:39,079
its sanction.

101
00:08:39,079 --> 00:08:48,170
The fact is: these laws cast a dark and paralysing
pall over everyone who ever felt like they

102
00:08:48,170 --> 00:08:49,500
were different.

103
00:08:49,500 --> 00:08:57,139
The fact is: these laws represented nothing
less than official, state-sanctioned homophobia.

104
00:08:57,139 --> 00:09:01,579
And we wonder why, Speaker – we wonder why
gay and lesbian and bi and trans teenagers

105
00:09:01,579 --> 00:09:05,779
are still the target of a red, hot hatred.

106
00:09:05,779 --> 00:09:10,610
We wonder why hundreds of thousands of Australians
are still formally excluded from something

107
00:09:10,610 --> 00:09:16,800
as basic and decent as a formal celebration
of love.

108
00:09:16,800 --> 00:09:21,980
And we wonder why so many people are still
forced to drape their lives in shame.

109
00:09:21,980 --> 00:09:30,439
Shame: that deeply personal condition, described
by Peter McEwan as “the feeling of not being

110
00:09:30,439 --> 00:09:31,470
good enough.”

111
00:09:31,470 --> 00:09:33,930
Peter was arrested in 1967.

112
00:09:33,930 --> 00:09:40,180
He soon fled overseas to escape that time in his life.

113
00:09:40,180 --> 00:09:45,699
The fourth man I spoke with last week, Tom
Anderson, met his own private terror when

114
00:09:45,699 --> 00:09:48,279
he was 14.

115
00:09:48,279 --> 00:09:55,490
For weeks, he was routinely sexually assaulted
by his employer, his boss – a man in his 40s.

116
00:09:55,490 --> 00:10:02,550
His parents, in all good faith, took Tom down
to the local police station to make a formal statement

117
00:09:55,490 --> 00:10:02,550
His parents, in all good faith, took Tom down
to the local police station to make a formal statement

118
00:10:02,550 --> 00:10:06,759
and get his employer, this predator, charged.

119
00:10:06,759 --> 00:10:08,439
And he was.

120
00:10:08,439 --> 00:10:12,990
But so was Tom.

121
00:10:12,990 --> 00:10:17,269
This child victim of sexual assault was charged
with one count of buggery and two counts of

122
00:10:17,269 --> 00:10:19,870
gross indecency.

123
00:10:19,870 --> 00:10:25,699
Can you believe, Speaker, that the year was
1977?

124
00:10:25,699 --> 00:10:31,129
Today, Tom carries with him a quiet bravery
that is hard to put into words.

125
00:10:31,129 --> 00:10:36,790
And he told me about the time – one day,
just a few years ago – when his home was

126
00:10:36,790 --> 00:10:37,540
burgled.

127
00:10:37,540 --> 00:10:42,930
“I’m a grown man”, he said, “but the
moment the police came around to inspect the

128
00:10:42,930 --> 00:10:48,499
house, and I opened the door…I became that
14-year-old boy again.”

129
00:10:48,499 --> 00:10:57,749
“I couldn’t talk. I was frozen. I was
a grown man and I couldn’t talk.”

130
00:10:57,749 --> 00:11:01,319
This was life for innocent people like Tom.

131
00:11:01,319 --> 00:11:05,699
We told them they were fugitives living outside
the law.

132
00:11:05,699 --> 00:11:10,139
We gave them no safe place to find themselves
– or find each other.

133
00:11:10,139 --> 00:11:15,499
And we made sure they couldn’t trust a soul. Not even their family.

134
00:11:15,500 --> 00:11:20,439
A life like that. What do you think that does
to a human being?

135
00:11:20,439 --> 00:11:25,189
What do you think it does to their ability
to find purpose, to hold themselves with confidence

136
00:11:25,189 --> 00:11:30,089
to be happy, to be social, to be free?

137
00:11:30,089 --> 00:11:36,160
Don’t tell me that these laws were simply
a suppression of sex.

138
00:11:36,160 --> 00:11:38,139
This was a suppression of spirit.

139
00:11:38,139 --> 00:11:40,699
A denial of love.

140
00:11:40,699 --> 00:11:42,519
And it lives on, today.

141
00:11:42,519 --> 00:11:48,279
While the laws were terminated in the 1980s,
they still remain next to the names of so many men.

142
00:11:48,280 --> 00:11:54,860
Most of them are now gone, but a criminal
conviction engraved upon their place in history.

143
00:11:54,870 --> 00:12:01,150
I can inform the House and all honourable members that six men have now successfully applied to expunge these

144
00:12:01,150 --> 00:12:04,339
convictions from their record.

145
00:12:04,339 --> 00:12:06,379
Many more have begun the process.

146
00:12:06,379 --> 00:12:13,889
This won’t erase the injustice, but it is
an accurate statement of what I believe today:

147
00:12:13,889 --> 00:12:17,920
That these convictions should never have happened.

148
00:12:17,920 --> 00:12:23,059
That the charges will be deleted, as if they
had never existed.

149
00:12:23,059 --> 00:12:30,079
And that their subjects can call themselves,
once again, law-abiding Victorians.

150
00:12:30,079 --> 00:12:35,740
Expungement is one thing, but these victims
won’t find their salvation in this alone.

151
00:12:35,740 --> 00:12:41,410
They are each and every one of them owed hope.

152
00:12:41,410 --> 00:12:45,809
And all four of the men I met told me they
only began to find that hope when they met

153
00:12:45,809 --> 00:12:47,850
people who were just like them.

154
00:12:47,850 --> 00:12:52,980
Peter McEwan – back in the country, and
emerging from years of shame – started meeting

155
00:12:52,980 --> 00:12:58,139
weekly with some gay friends at university
in 1972.

156
00:12:58,139 --> 00:13:06,230
“We realised we were all outlaws together,”
he said, “and we learnt to say that we are

157
00:13:06,230 --> 00:13:07,019
good”.

158
00:13:07,019 --> 00:13:13,149
“We learnt to say ‘black is beautiful,
women are strong – and gay is good.’”

159
00:13:13,149 --> 00:13:19,939
“Once I learnt I was good, it led me to
question everyone who said I was evil and

160
00:13:19,939 --> 00:13:20,339
sick.”

161
00:13:20,339 --> 00:13:27,709
“Gay men had taken on board the shame. Through
each other we found our pride.”

162
00:13:27,709 --> 00:13:36,660
Then he paused for a second — and I will carry this with me forever — he paused for a second and he said to me:

163
00:13:36,660 --> 00:13:39,860
“Pride is the opposite of shame.”

164
00:13:39,869 --> 00:13:42,490
He’s right.

165
00:13:42,490 --> 00:13:45,629
Pride is not a cold acceptance; it’s a celebration.

166
00:13:45,629 --> 00:13:49,879
It’s about wearing your colours and baring
your character.

167
00:13:49,879 --> 00:13:53,800
The mere expression of pride was an act of
sheer defiance.

168
00:13:53,800 --> 00:13:59,290
These people we speak about – they weren’t
just fighting for the right to be equal.

169
00:13:59,290 --> 00:14:03,160
They were fighting for the right to be different.

170
00:14:03,160 --> 00:14:09,499
And I want everyone in this state, young or
old, to know that you, too, have that right.

171
00:14:09,499 --> 00:14:10,850
You were born with that right.

172
00:14:10,850 --> 00:14:17,559
And being who you are is good enough for me
– and it’s good enough for all of us.

173
00:14:17,559 --> 00:14:22,009
Here in Victoria, equality is not negotiable.

174
00:14:22,009 --> 00:14:29,639
Here, you can be different from everybody
else, but still be treated the same as everybody else.

175
00:14:29,639 --> 00:14:31,610
Because we believe in fairness.

176
00:14:31,610 --> 00:14:36,290
We believe in honesty, too – so we have
to acknowledge this:

177
00:14:36,290 --> 00:14:39,540
For the time being, we can’t promise things
will be easy.

178
00:14:39,540 --> 00:14:42,939
Tomorrow, a young bloke will get hurt.

179
00:14:42,939 --> 00:14:47,439
Tomorrow, a parent will turn their back on
their child.

180
00:14:47,439 --> 00:14:54,249
Tomorrow, a loving couple and their beautiful
baby will be met with a stare of contempt.

181
00:14:54,249 --> 00:14:58,519
Tomorrow, a trans woman will be turned away
from a job interview.

182
00:14:58,519 --> 00:15:04,139
And tomorrow, a gay teenager will think about
ending his own life.

183
00:15:04,139 --> 00:15:05,509
That’s the truth.

184
00:15:05,509 --> 00:15:10,189
There is so much more we need to do to make
things right.

185
00:15:10,189 --> 00:15:18,820
Until then, we can’t promise things will
be easy. Far from it.

186
00:15:18,820 --> 00:15:23,520
We can’t guarantee that everyone in your
life will respect the way you want to live it.

187
00:15:23,529 --> 00:15:28,600
And we can’t expect you to make what must
be a terrifying plunge until you know the

188
00:15:28,600 --> 00:15:30,819
time is right.

189
00:15:30,819 --> 00:15:39,759
But just know that whenever that time comes,
you have a Government and a Parliament that’s on your side.

190
00:15:39,759 --> 00:15:43,429
You have a Government that is trying to make
the state a safer place – in the classroom,

191
00:15:43,429 --> 00:15:44,499
in the workplace.

192
00:15:44,499 --> 00:15:48,740
You have a Government that is trying to eradicate
a culture of bullying and harassment so that

193
00:15:48,740 --> 00:15:53,019
the next generation of children are never
old enough to experience it.

194
00:15:53,019 --> 00:15:58,970
You have a Government that sees these indisputable
statistics – of LGBTI self-harm, of suicide

195
00:15:58,970 --> 00:16:02,480
– and commits to their complete upheaval.

196
00:16:02,480 --> 00:16:06,490
You have a Government that believes you’re
free to be who you are, and to marry the person

197
00:16:06,490 --> 00:16:08,189
you love.

198
00:16:08,189 --> 00:16:12,519
And you have a Government that knows just
one life saved is worth all the effort.

199
00:16:12,519 --> 00:16:16,850
Speaker, as part of this process, I learnt
that two women were convicted for offensive

200
00:16:16,850 --> 00:16:22,069
behaviour in the 1970s for holding hands – on
a tram.

201
00:16:22,069 --> 00:16:24,350
So let me finish by saying this:

202
00:16:24,350 --> 00:16:29,470
If you are a member of the LGBTI community,
and there’s someone in your life that you

203
00:16:29,470 --> 00:16:33,420
love – a partner or a friend – then do
me a favour:

204
00:16:33,420 --> 00:16:37,249
Next time you’re on a tram in Melbourne,
hold their hand.

205
00:16:37,249 --> 00:16:40,610
Do it with pride and defiance.

206
00:16:40,610 --> 00:16:43,139
Because you have that freedom.

207
00:16:43,139 --> 00:16:49,420
And here in the progressive capital of our nation, I can
think of nothing more Victorian than that.

208
00:16:49,420 --> 00:16:54,040
Speaker, it’s been a life of struggle for
generations of Victorians.

209
00:16:54,040 --> 00:16:58,749
As representatives, we take full responsibility.

210
00:16:58,749 --> 00:17:05,170
We criminalised homosexual thoughts and deeds.
We validated homophobic words and acts.

211
00:17:05,170 --> 00:17:11,410
And we set the tone for a society that ruthlessly
punished the different – with a short sentence

212
00:17:11,410 --> 00:17:15,970
in prison, and a life sentence of shame.

213
00:17:15,970 --> 00:17:18,339
From now on, that shame is ours.

214
00:17:18,339 --> 00:17:22,910
This Parliament and this Government are to
be formally held to account for designing

215
00:17:22,910 --> 00:17:24,790
a culture of darkness and shame.

216
00:17:24,790 --> 00:17:30,700
And those who faced its sanction, and lived
in fear, are to be formally recognised for

217
00:17:30,700 --> 00:17:34,580
their relentless pursuit of freedom and love.

218
00:17:34,580 --> 00:17:38,530
It all started here, and it all ends here, too.

219
00:17:38,530 --> 00:17:42,800
To our knowledge, no jurisdiction in the world
has ever offered a full and formal apology

220
00:17:42,800 --> 00:17:45,100
for laws like these.

221
00:17:45,100 --> 00:17:49,860
So please, let these words rest forever in
our records:

222
00:17:49,860 --> 00:17:54,650
On behalf of the Parliament, the Government
and the people of Victoria.

223
00:17:54,650 --> 00:17:57,090
For the laws we passed.

224
00:17:57,090 --> 00:17:59,590
And the lives we ruined.

225
00:17:59,590 --> 00:18:02,050
And the standards we set.

226
00:18:02,050 --> 00:18:10,280
We are so sorry. Humbly, deeply, sorry.

