1
00:00:00,220 --> 00:00:04,759
My name is Amie. I was a local resident of
the arteries of Melbourne and I was a student

2
00:00:04,759 --> 00:00:09,920
at the Lilydale TAFE campus when it closed
in 2013. When I found out that I wanted to keep

3
00:00:09,920 --> 00:00:14,280
studying in my field and I needed to move
out of home, I was really angry about it.

4
00:00:14,280 --> 00:00:19,119
So I contacted my fellow students and we started
a campaign. I wanted to show how important

5
00:00:19,119 --> 00:00:24,320
a local, accessible TAFE would be to the community
out here. It was hard work, but people really

6
00:00:24,320 --> 00:00:29,150
got behind us and it was awesome to see. And
this October, after two years, we finally

7
00:00:29,150 --> 00:00:33,450
did it. The Lilydale TAFE will reopen. I'm
so happy that this government listened to

8
00:00:33,450 --> 00:00:37,560
the locals and kept its promise. It means
that in the future, kids from the arteries

9
00:00:37,560 --> 00:00:42,220
will be able to attend their local TAFE, which
is just fantastic.

