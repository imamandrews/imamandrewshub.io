1
00:00:04,380 --> 00:00:08,960
Melbourne's new Metro Tunnel will free up
space in the city loop so we can run more

2
00:00:08,960 --> 00:00:11,950
trains in and out of the CBD.

3
00:00:11,950 --> 00:00:16,940
Works on the biggest public transport project
in Victoria's history are underway.

4
00:00:16,940 --> 00:00:23,470
Up to 100 utilities including gas, power and
telecommunications services need to be relocated

5
00:00:23,470 --> 00:00:24,829
or protected.

6
00:00:24,829 --> 00:00:28,110
Some changes to the road network will be required.

7
00:00:28,110 --> 00:00:32,640
A number of network enhancement projects will
be delivered in conjunction with existing

8
00:00:32,640 --> 00:00:37,700
Vic Roads initiatives to accommodate diverted
traffic while Metro Tunnel is built.

9
00:00:37,700 --> 00:00:43,180
We'll start excavating station shafts and
clearing areas next to Swanston Street to

10
00:00:43,180 --> 00:00:47,620
prepare for the construction of the two new
underground city stations.

11
00:00:47,620 --> 00:00:55,940
The face of our city will change from early
2017 as we dig 11 storys below the CBD.

12
00:00:55,940 --> 00:00:59,850
Metro Tunnel will create the international-style
train system we need.

