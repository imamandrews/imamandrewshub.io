1
00:00:00,240 --> 00:00:04,720
The State Government is funding the Mernda Rail extension project.

2
00:00:04,720 --> 00:00:07,980
From South Morang Station, the railway will continue

3
00:00:08,160 --> 00:00:11,280
for approximately 8 kilometres to Mernda.

4
00:00:12,260 --> 00:00:14,800
Most of the railway will travel along the ground.

5
00:00:15,900 --> 00:00:19,400
A new walking and cycling path, along with several crossings

6
00:00:19,620 --> 00:00:23,240
will open up the area, complementing City of Whittlesea's

7
00:00:23,240 --> 00:00:27,620
proposed path nework, eventually creating a continuous link

8
00:00:27,620 --> 00:00:30,020
along the corridor from South Morang to Mernda.

9
00:00:30,200 --> 00:00:33,880
A station will be built near Marymede Catholic College

10
00:00:33,880 --> 00:00:37,020
with up to 500 car parks, and a kiss-and-ride.

11
00:00:37,940 --> 00:00:41,340
As part of the project, bidders will be required to investigate

12
00:00:41,340 --> 00:00:45,460
and price the proposed station near Hawkstowe Parade.

13
00:00:45,840 --> 00:00:50,580
The bridge over Simon Creek, the Yan Yean Bridge, the Pine Track, and the Parkway

14
00:00:50,740 --> 00:00:53,900
will protect the creek and the surrounding local environment

15
00:00:53,900 --> 00:00:57,020
providing connectivity. The premium station at Mernda

16
00:00:57,160 --> 00:01:00,460
will be located on the south side of Bridge Inn Road,

17
00:01:00,560 --> 00:01:05,360
and will integrate with a proposed town centre and the transport hub.

