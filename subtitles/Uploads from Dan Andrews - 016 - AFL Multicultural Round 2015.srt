1
00:00:00,000 --> 00:00:04,200
the AFL multicultural round brings

2
00:00:01,919 --> 00:00:05,128
together our greatest asset and our

3
00:00:04,200 --> 00:00:08,999
greatest game we love our sport not only

4
00:00:07,049 --> 00:00:11,518
on the big stage but also in our

5
00:00:09,000 --> 00:00:14,969
neighborhoods our local clubs our parks

6
00:00:11,519 --> 00:00:17,850
and backyards sport and any level brings

7
00:00:14,968 --> 00:00:20,128
us together as a community and ours is a

8
00:00:17,850 --> 00:00:23,279
palace diverse a community as you can

9
00:00:20,129 --> 00:00:26,278
find its new Australians and Australians

10
00:00:23,278 --> 00:00:27,867
have been here for 60 thousand years and

11
00:00:26,278 --> 00:00:30,858
it's something we have to embrace

12
00:00:27,868 --> 00:00:33,178
because it's a part of who we are

13
00:00:30,859 --> 00:00:36,328
everyone here has a right to belong and

14
00:00:33,179 --> 00:00:39,388
be included and everyone should embrace

15
00:00:36,329 --> 00:00:41,389
diversity I thank the AFL and everybody

16
00:00:39,390 --> 00:00:45,649
who's involved in this great game

17
00:00:41,390 --> 00:00:45,649
serving this great purpose

18
00:00:49,039 --> 00:00:53,989
Not authorised by the Victorian Government 1

19
00:00:51,299 --> 00:00:53,989
Treasury place Melbourne

