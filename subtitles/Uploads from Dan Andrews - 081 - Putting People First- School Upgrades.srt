1
00:00:00,000 --> 00:00:05,700
well if we want to prepare students for

2
00:00:02,730 --> 00:00:06,108
the future the future for work then this

3
00:00:05,700 --> 00:00:08,838
is where it all begins

4
00:00:10,339 --> 00:00:14,149
on the assistant principal here at

5
00:00:12,080 --> 00:00:16,070
lyndale secondary college at current

6
00:00:14,150 --> 00:00:18,860
buildings date back to nineteen sixty

7
00:00:16,070 --> 00:00:21,489
one they've basically been wearing out

8
00:00:18,859 --> 00:00:24,049
they built for different time and place

9
00:00:21,489 --> 00:00:27,288
currently our instrumental music staff

10
00:00:24,050 --> 00:00:29,660
operate out of portables education moved

11
00:00:27,289 --> 00:00:31,760
on since then so we've got building

12
00:00:29,660 --> 00:00:34,400
approval for stem building which

13
00:00:31,760 --> 00:00:36,979
incorporates science technology

14
00:00:34,399 --> 00:00:38,899
engineering and maths and for performing

15
00:00:36,979 --> 00:00:40,969
arts building a new canteen and you've

16
00:00:38,899 --> 00:00:43,339
seen your study center there's been a

17
00:00:40,969 --> 00:00:45,228
lot of positive thoughts regarding you

18
00:00:43,340 --> 00:00:48,019
know James molino and Daniel Andrews

19
00:00:45,229 --> 00:00:50,599
promoting Victoria's the education state

20
00:00:48,020 --> 00:00:54,640
we're looking forward to buildings that

21
00:00:50,600 --> 00:00:54,640
actually cater for 21st century learners

22
00:00:55,340 --> 00:01:00,259
Not authorised by the Victorian Government 1

23
00:00:57,539 --> 00:01:00,259
Treasury place Melbourne

