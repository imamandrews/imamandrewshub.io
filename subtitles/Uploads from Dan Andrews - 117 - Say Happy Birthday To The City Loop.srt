1
00:00:04,230 --> 00:00:06,639
[Music]

2
00:00:07,549 --> 00:00:10,867
[Applause]

3
00:00:14,240 --> 00:00:19,039
[Music]

4
00:00:15,160 --> 00:00:19,039
[Applause]

5
00:00:23,670 --> 00:00:27,039
[Applause]

6
00:00:30,199 --> 00:00:43,977
[Applause]

7
00:00:49,170 --> 00:00:56,389
[Music]

8
00:01:06,530 --> 00:01:08,869
you

9
00:01:08,140 --> 00:01:12,130
[Applause]

10
00:01:08,870 --> 00:01:19,340
[Music]

11
00:01:12,129 --> 00:01:19,338
[Applause]

12
00:01:20,719 --> 00:01:39,118
[Music]

13
00:01:35,810 --> 00:01:41,909
melbourne garden city of the garden

14
00:01:39,120 --> 00:01:44,719
state an orderly city of charm and

15
00:01:41,909 --> 00:01:51,867
beauty a busy city of progress and

16
00:01:44,719 --> 00:01:55,248
people it is a big city big in size big

17
00:01:51,868 --> 00:01:57,959
in population but it has its problems

18
00:01:55,250 --> 00:02:01,160
moving people to and from work and

19
00:01:57,959 --> 00:02:03,749
around the city every day is one of them

20
00:02:01,159 --> 00:02:06,199
more and more people placing more and

21
00:02:03,750 --> 00:02:09,958
more strain on transport systems

22
00:02:06,200 --> 00:02:13,259
overcrowded trams trains and buses daily

23
00:02:09,959 --> 00:02:16,069
traffic jams the rush rush rush to beat

24
00:02:13,259 --> 00:02:20,339
the clock the frustration of holdups

25
00:02:16,069 --> 00:02:22,378
congestion the surface affected beneath

26
00:02:20,340 --> 00:02:24,689
this sprawling severe drama is taking

27
00:02:22,379 --> 00:02:27,058
place a drama which will reshape the

28
00:02:24,689 --> 00:02:31,108
Metropolitan transport system the

29
00:02:27,060 --> 00:02:33,029
Melbourne underground rail loop it's

30
00:02:31,110 --> 00:02:35,129
vital purpose is to expand present

31
00:02:33,030 --> 00:02:37,530
facilities not just to meet the needs of

32
00:02:35,129 --> 00:02:41,938
today but to cope with the demands of

33
00:02:37,530 --> 00:02:43,949
tomorrow the loop is not a separate

34
00:02:41,939 --> 00:02:45,208
railway under the city but an

35
00:02:43,949 --> 00:02:48,659
underground extension of the existing

36
00:02:45,209 --> 00:02:51,899
suburban rail system there will be three

37
00:02:48,659 --> 00:02:54,479
underground stations Parliament Museum

38
00:02:51,900 --> 00:02:56,010
and Flagstaff linked to Spencer Street

39
00:02:54,479 --> 00:02:58,679
and Flinders Street princes bridge

40
00:02:56,009 --> 00:03:00,839
stations all suburban tracks except the

41
00:02:58,680 --> 00:03:02,519
some Kilda and Port Melbourne lines will

42
00:03:00,840 --> 00:03:05,578
have direct access to the loop tracks

43
00:03:02,519 --> 00:03:07,919
and in addition there will be city

44
00:03:05,579 --> 00:03:09,459
circle trains providing a continuous

45
00:03:07,919 --> 00:03:19,448
service around the loop

46
00:03:09,460 --> 00:03:22,188
[Music]

47
00:03:19,449 --> 00:03:24,348
train travelers said by five city

48
00:03:22,188 --> 00:03:26,688
stations will have convenient access to

49
00:03:24,348 --> 00:03:30,399
business and commercial houses and shops

50
00:03:26,688 --> 00:03:30,398
in the city and adjacent districts

51
00:03:30,818 --> 00:03:36,318
designing planning and construction is

52
00:03:33,949 --> 00:03:38,508
mammoth this is a cut and cover

53
00:03:36,318 --> 00:03:40,518
operation where a gigantic pit has been

54
00:03:38,509 --> 00:03:42,108
dug from the surface the removal of

55
00:03:40,519 --> 00:03:43,849
buildings and the diversion of traffic

56
00:03:42,109 --> 00:03:46,158
was only the beginning to make way for

57
00:03:43,848 --> 00:03:48,649
the engineers and a task force of

58
00:03:46,158 --> 00:03:52,268
workers for making a complex ambitious

59
00:03:48,650 --> 00:03:55,688
and major investment into a reality

60
00:03:52,269 --> 00:03:59,448
construction of the loop started in 1971

61
00:03:55,688 --> 00:04:01,697
it is changing the face of mobile tens

62
00:03:59,449 --> 00:04:04,040
of thousands of tons of Earth have been

63
00:04:01,699 --> 00:04:07,780
gouged for the museum station site to

64
00:04:04,039 --> 00:04:07,779
make way for foundations and buildings

65
00:04:11,530 --> 00:04:14,638
[Music]

66
00:04:18,100 --> 00:04:24,109
the look is not simply one underground

67
00:04:21,079 --> 00:04:26,569
channel there are for each linking the

68
00:04:24,110 --> 00:04:29,360
five city stations and designed to serve

69
00:04:26,569 --> 00:04:31,269
a specific group of suburban railway

70
00:04:29,360 --> 00:04:34,449
lines

71
00:04:31,269 --> 00:04:34,449
[Applause]

72
00:04:54,579 --> 00:04:59,609
already real shapes are emerging

73
00:05:00,300 --> 00:05:08,710
[Applause]

74
00:05:05,589 --> 00:05:10,358
behind the frontline troops the 1,500

75
00:05:08,709 --> 00:05:12,939
workers who are mining and building

76
00:05:10,360 --> 00:05:15,610
tunneling and constructing are the

77
00:05:12,939 --> 00:05:17,528
backroom boys the engineers the

78
00:05:15,610 --> 00:05:20,529
architects and draftsman plan every

79
00:05:17,529 --> 00:05:23,378
detail and supervise every step with

80
00:05:20,529 --> 00:05:44,129
intricate care and infinite patience

81
00:05:23,379 --> 00:05:46,778
[Music]

82
00:05:44,129 --> 00:05:49,629
there's a universal fascination in

83
00:05:46,779 --> 00:05:52,059
trains model or life size their

84
00:05:49,629 --> 00:05:54,789
assassination - in the integration of a

85
00:05:52,060 --> 00:06:01,179
ground-level rail system and one deep

86
00:05:54,790 --> 00:06:02,979
below the surface the top end of column

87
00:06:01,180 --> 00:06:05,529
Street is seeing changes but around the

88
00:06:02,978 --> 00:06:08,109
corner 38 meters below Spring Street

89
00:06:05,529 --> 00:06:09,908
change is even more spectacular with

90
00:06:08,110 --> 00:06:14,039
construction of the lower platforms of

91
00:06:09,910 --> 00:06:16,419
Parliament Station well advanced an

92
00:06:14,038 --> 00:06:18,278
architects model highlights features of

93
00:06:16,418 --> 00:06:20,709
one entry to the city's transport system

94
00:06:18,279 --> 00:06:23,168
a huge and vital investment in the

95
00:06:20,709 --> 00:06:25,838
future an investment which will produce

96
00:06:23,168 --> 00:06:26,818
dividends of streamline travel for the

97
00:06:25,839 --> 00:06:31,569
people of Melbourne

98
00:06:26,819 --> 00:06:31,569
[Music]

99
00:06:32,100 --> 00:06:37,810
stations are as long as a city block so

100
00:06:35,560 --> 00:06:39,899
placement of entrances and exits must

101
00:06:37,810 --> 00:06:43,689
suit the convenience of travellers

102
00:06:39,899 --> 00:06:43,689
[Music]

103
00:06:44,870 --> 00:06:52,520
[Applause]

104
00:06:49,600 --> 00:06:54,560
the Minister for transport and the

105
00:06:52,519 --> 00:06:57,229
chairman of the authority are frequent

106
00:06:54,560 --> 00:06:58,760
visitors to the project progress can

107
00:06:57,230 --> 00:07:01,960
readily be inspected from one of the

108
00:06:58,759 --> 00:07:01,959
narrow gauge work trains

109
00:07:02,240 --> 00:07:06,220
[Applause]

110
00:07:09,209 --> 00:07:16,947
[Music]

111
00:07:09,759 --> 00:07:16,948
[Applause]

112
00:07:17,269 --> 00:07:25,329
[Music]

113
00:07:17,910 --> 00:07:27,650
[Applause]

114
00:07:25,329 --> 00:07:30,048
Flinders Street is seeing its share of

115
00:07:27,649 --> 00:07:33,128
change including an overpass for two

116
00:07:30,050 --> 00:07:33,129
additional rail tracks

117
00:07:44,250 --> 00:07:50,259
[Music]

118
00:07:47,470 --> 00:07:53,200
Flagstaff station progresses platforms

119
00:07:50,259 --> 00:07:55,379
the escalators concourses stairs and

120
00:07:53,199 --> 00:07:55,379
reps

121
00:07:58,189 --> 00:08:15,007
[Applause]

122
00:08:25,550 --> 00:08:31,200
[Music]

123
00:08:35,590 --> 00:08:41,450
deep underground engineering skill

124
00:08:38,740 --> 00:08:42,949
reflected in rugged symmetry competes

125
00:08:41,450 --> 00:08:45,940
for attention with the work of the

126
00:08:42,950 --> 00:08:45,940
artist on the surface

127
00:08:46,179 --> 00:08:51,619
Moomba a time for getting together and

128
00:08:49,429 --> 00:08:54,139
having fun for looking at the work of

129
00:08:51,620 --> 00:08:55,269
local artists for spending leisure time

130
00:08:54,139 --> 00:09:00,028
and the bright sun

131
00:08:55,269 --> 00:09:00,028
[Music]

132
00:09:05,649 --> 00:09:11,868
only meters away in the darkness below

133
00:09:09,309 --> 00:09:14,089
bro taking pick machines with teeth

134
00:09:11,870 --> 00:09:15,919
harder than steel continue an

135
00:09:14,090 --> 00:09:18,399
unrelenting drive toward the day of

136
00:09:15,919 --> 00:09:18,399
completion

137
00:09:30,970 --> 00:09:37,189
the staccato of pneumatic rules shatters

138
00:09:33,889 --> 00:09:41,379
the Uranus as man and his tools come to

139
00:09:37,190 --> 00:09:41,380
grips with the Earth's solid toughness

140
00:09:42,100 --> 00:09:47,029
built in Melbourne the full-face

141
00:09:44,778 --> 00:09:49,818
tunnel-boring machine aptly called the

142
00:09:47,028 --> 00:09:52,758
mole uses its 60 disk cutters to

143
00:09:49,818 --> 00:09:55,768
excavate a 7 meter circle in solid rock

144
00:09:52,759 --> 00:10:04,849
at every turn that's giant cut ahead

145
00:09:55,769 --> 00:10:08,108
[Music]

146
00:10:04,850 --> 00:10:08,108
[Applause]

147
00:10:11,279 --> 00:10:16,389
breakthrough with shattering force in a

148
00:10:14,169 --> 00:10:18,558
crash of rock the mole breaks through to

149
00:10:16,389 --> 00:10:31,219
the draft relief shot

150
00:10:18,559 --> 00:10:31,219
[Music]

151
00:10:31,830 --> 00:10:35,269
[Applause]

152
00:10:37,139 --> 00:10:42,249
there is much to be done with the

153
00:10:40,480 --> 00:10:44,499
flexibility provided by the loop and

154
00:10:42,250 --> 00:10:46,719
extra tracks on suburban lines

155
00:10:44,500 --> 00:10:48,460
greatly increased capacity will be

156
00:10:46,720 --> 00:10:52,359
available to the Melbourne railway

157
00:10:48,460 --> 00:10:54,879
system more expressive reduction in

158
00:10:52,360 --> 00:10:57,449
travel time better and faster train

159
00:10:54,879 --> 00:10:57,448
services

160
00:10:58,700 --> 00:11:06,280
there is overhead wiring to be installed

161
00:11:02,289 --> 00:11:06,279
retaining walls to be built

162
00:11:08,789 --> 00:11:20,279
signal gantry is to be erected ramps and

163
00:11:17,039 --> 00:11:23,689
box tunnels to be completed tracks to be

164
00:11:20,279 --> 00:11:25,989
layed more rolling stock to be built

165
00:11:23,690 --> 00:11:29,109
[Applause]

166
00:11:25,990 --> 00:11:33,129
additional communications equipment in

167
00:11:29,110 --> 00:11:34,900
an era of electronics the project is an

168
00:11:33,129 --> 00:11:37,298
exemplification of cooperation and

169
00:11:34,899 --> 00:11:39,729
coordination on a grand scale between

170
00:11:37,299 --> 00:11:42,098
one and a half thousand people of many

171
00:11:39,730 --> 00:11:50,399
disciplines

172
00:11:42,100 --> 00:11:50,400
[Applause]

173
00:11:53,799 --> 00:12:04,418
a glimpse of the future below the

174
00:12:01,539 --> 00:12:06,008
surface for members of the public during

175
00:12:04,419 --> 00:12:08,178
a Melbourne underground rail loop

176
00:12:06,009 --> 00:12:11,579
Authority open day

177
00:12:08,179 --> 00:12:11,578
[Music]

178
00:12:12,389 --> 00:12:17,799
[Applause]

179
00:12:15,149 --> 00:12:21,500
above ground there is ample evidence of

180
00:12:17,799 --> 00:12:24,079
activity and the pattern of programs

181
00:12:21,500 --> 00:12:30,679
[Applause]

182
00:12:24,080 --> 00:12:30,679
[Music]

183
00:12:38,509 --> 00:12:45,009
machines Rumble and digg'd invite men

184
00:12:41,909 --> 00:12:47,708
plan and strive and sweat

185
00:12:45,009 --> 00:12:49,989
[Music]

186
00:12:47,710 --> 00:12:52,420
around the clock the unremitting task

187
00:12:49,990 --> 00:12:55,089
goes on towards the reward of conclusion

188
00:12:52,419 --> 00:12:56,527
and completion in the early 1980s by the

189
00:12:55,089 --> 00:12:59,109
Melbourne underground rail loop

190
00:12:56,528 --> 00:13:01,718
authority of an underworld artery which

191
00:12:59,110 --> 00:13:04,589
will carry moblins railway system into

192
00:13:01,720 --> 00:13:04,589
the 21st century

193
00:13:07,129 --> 00:13:10,209
[Music]

194
00:13:33,789 --> 00:13:35,849
you

