1
00:00:04,170 --> 00:00:08,910
Building a station in a major city is like
open heart surgery. You've got to keep the

2
00:00:08,910 --> 00:00:14,889
body moving, the city moving, the blood pumping.
We can't just stop everything dead, so we're

3
00:00:14,889 --> 00:00:16,869
keeping the city moving as much as possible.

4
00:00:17,080 --> 00:00:19,640
To manage traffic disruptions during construction,

5
00:00:19,650 --> 00:00:23,850
we'll be working with VicRoads and the City
of Melbourne on diversion strategies to get

6
00:00:23,850 --> 00:00:28,070
people around to where they want to go. Obviously
on a project of this scale, there's some potential

7
00:00:28,070 --> 00:00:33,390
for short-term closures from time to time.
La Trobe Street, Collins Street, Bourke Street

8
00:00:33,390 --> 00:00:36,740
trams will continue to operate as they do
today, and at Flinders Street, there will

9
00:00:36,740 --> 00:00:38,179
need to be some closures. There will be some
disruptions on the likes of Flinders Street,

10
00:00:38,179 --> 00:00:41,559
There will be some disruptions
on the likes of Flinders Street,

11
00:00:41,559 --> 00:00:46,080
where we're connecting the CBD South Station
to Flinders Street Station. We'll be occupying

12
00:00:46,080 --> 00:00:49,350
City Square for a number of years. Those are
sort of the disruptions that we'll be seeing

13
00:00:49,350 --> 00:00:54,390
at CBD South. At CBD North, there'll be a
lot of work happening within Franklin Street.

14
00:00:54,390 --> 00:00:58,070
Alternatives will be put in place to make
sure people can still get around the city

15
00:00:58,070 --> 00:00:59,650
as they do today.

