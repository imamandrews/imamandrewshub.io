1
00:00:03,290 --> 00:00:06,040
there's no doubt at all this is an

2
00:00:06,089 --> 00:00:07,020
iconic building it's a great space in

3
00:00:09,330 --> 00:00:14,969
the center of our city it is a fantastic

4
00:00:13,109 --> 00:00:16,858
precinct but one that needs to be so

5
00:00:14,968 --> 00:00:19,889
much better this is a grand old building

6
00:00:16,859 --> 00:00:21,420
that has been allowed to really crumble

7
00:00:19,890 --> 00:00:22,830
and we have to do much better than that

8
00:00:21,420 --> 00:00:24,210
that's why I'm so very proud today to

9
00:00:22,829 --> 00:00:26,338
announce that our government will invest

10
00:00:24,210 --> 00:00:28,140
a hundred million dollars to both

11
00:00:26,339 --> 00:00:29,968
complete heritage works to bring this

12
00:00:28,140 --> 00:00:32,489
grand old building back to its former

13
00:00:29,969 --> 00:00:36,929
glory back to the iconic status that we

14
00:00:32,488 --> 00:00:38,758
know it needs to enjoy but also a very

15
00:00:36,929 --> 00:00:40,468
significant investment in making sure

16
00:00:38,759 --> 00:00:43,289
that the two hundred thousand Victorians

17
00:00:40,469 --> 00:00:45,690
who use this station this precinct every

18
00:00:43,289 --> 00:00:48,418
single day have a much better public

19
00:00:45,689 --> 00:00:50,189
transport experience it's about the

20
00:00:48,420 --> 00:00:51,808
basics it's about focusing on things

21
00:00:50,189 --> 00:00:55,979
that make such a big difference

22
00:00:51,808 --> 00:00:58,619
signage the surfacing the finish of the

23
00:00:55,979 --> 00:01:00,269
platforms the ability to find your way

24
00:00:58,619 --> 00:01:02,159
around the station the ability to use

25
00:01:00,270 --> 00:01:03,718
toilets that are of an appropriate

26
00:01:02,159 --> 00:01:05,849
standard they're just not at the moment

27
00:01:03,719 --> 00:01:08,549
the roof leaks we've got water damage

28
00:01:05,849 --> 00:01:10,259
right throughout the whole building we

29
00:01:08,549 --> 00:01:12,779
need to make these investments to make

30
00:01:10,260 --> 00:01:14,849
sure that this is a better facility a

31
00:01:12,780 --> 00:01:18,180
better experience for public transport

32
00:01:14,849 --> 00:01:23,718
users but also to return this grand old

33
00:01:18,180 --> 00:01:23,720
building this icon so its former glory

34
00:01:24,108 --> 00:01:29,279
around 105 years ago this building

35
00:01:26,879 --> 00:01:31,589
opened as the most modern piece of

36
00:01:29,280 --> 00:01:33,688
architecture of its time as a fully

37
00:01:31,590 --> 00:01:36,298
functioning railway station for the city

38
00:01:33,688 --> 00:01:38,758
of Melbourne we are so fortunate more

39
00:01:36,299 --> 00:01:41,218
than a century later to continue to have

40
00:01:38,759 --> 00:01:43,590
such a wonderful asset for our city

41
00:01:41,218 --> 00:01:45,718
still being used for the purpose it was

42
00:01:43,590 --> 00:01:48,139
designed for as a fully functioning

43
00:01:45,718 --> 00:01:50,578
transport hub a hub for our rail network

44
00:01:48,140 --> 00:01:53,100
here into Melbourne and across the

45
00:01:50,578 --> 00:01:55,978
suburbs there is a lot of work to be

46
00:01:53,099 --> 00:01:58,198
done to bring back some of the former

47
00:01:55,978 --> 00:02:01,378
glory of this of this building we also

48
00:01:58,200 --> 00:02:03,390
recognize that this is a facility that's

49
00:02:01,379 --> 00:02:06,449
very much fit for purpose as a public

50
00:02:03,390 --> 00:02:07,949
transport hub and that's why significant

51
00:02:06,450 --> 00:02:10,828
amount of this funding is going to be

52
00:02:07,950 --> 00:02:13,680
invested in improving the passenger

53
00:02:10,829 --> 00:02:15,929
experience improving the lighting

54
00:02:13,680 --> 00:02:17,910
particularly in the in the underneath

55
00:02:15,930 --> 00:02:20,670
passageways and improving the

56
00:02:17,909 --> 00:02:23,038
information that is provided to

57
00:02:20,669 --> 00:02:24,779
passengers and we're very pleased to be

58
00:02:23,039 --> 00:02:28,729
able to breathe new life and a new

59
00:02:24,780 --> 00:02:28,730
beginning into Flinders Street Station

60
00:02:35,530 --> 00:02:37,590
you

