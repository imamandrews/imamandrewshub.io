1
00:00:00,000 --> 00:00:02,700
My name's Krystal Wright and I'm 27 years old

2
00:00:02,720 --> 00:00:05,960
and I was diagnosed with stage two breast cancer

3
00:00:05,960 --> 00:00:08,680
in November last year, 2015.

4
00:00:08,680 --> 00:00:13,720
Obviously being 27 and being diagnosed with cancer was quite a shock.

5
00:00:13,720 --> 00:00:18,380
I've heard nothing but fantastic stories of the staff and the doctors,

6
00:00:18,380 --> 00:00:22,820
so it was easy to decide to get Peter Mac to help me out.

7
00:00:22,820 --> 00:00:26,400
SHERENE: The interaction I think between scientists and clinicians are really important

8
00:00:26,400 --> 00:00:29,920
to help drive forward the science and to try and bring that to patients.

9
00:00:29,920 --> 00:00:35,180
We can be right there with the patient and literally come up to the 12th floor to process the sample.

10
00:00:35,180 --> 00:00:39,900
And then when we process the sample, we hope to understand what's making that breast cancer grow.

11
00:00:39,900 --> 00:00:42,620
And we can potentially then enrol that patient onto a clinical trial.

12
00:00:42,620 --> 00:00:47,060
And we're really the only centre in the southern hemisphere that has this capability.

13
00:00:47,140 --> 00:00:51,900
KRYSTAL: My team of surgeons and oncologists, nurses,

14
00:00:51,900 --> 00:00:54,180
they make it so much easier.

15
00:00:54,180 --> 00:00:55,740
It's not as daunting.

16
00:00:55,740 --> 00:00:58,700
Yeah, it's still scary, not knowing what to expect,

17
00:00:58,700 --> 00:01:05,200
but having a team that everyone gets it makes the experience so much easier.

18
00:01:05,560 --> 00:01:08,140
BOON: When I see Krystal in 10 years' time and

19
00:01:08,140 --> 00:01:12,220
she's well and healthy with a family,

20
00:01:12,220 --> 00:01:15,740
I think that would be the best reward I could get.

