1
00:00:00,269 --> 00:00:04,980
firstly can I thank you very much for

2
00:00:02,240 --> 00:00:05,139
your warm welcome it is a great honor a

3
00:00:05,250 --> 00:00:08,399
privilege to be here today I want to

4
00:00:07,378 --> 00:00:09,927
begin my remarks by of course

5
00:00:08,400 --> 00:00:11,670
acknowledging the traditional owners of

6
00:00:09,929 --> 00:00:13,348
this land and poem I do personal

7
00:00:11,669 --> 00:00:15,597
respects to elder's past the president

8
00:00:13,349 --> 00:00:19,160
to each and every member of Aboriginal

9
00:00:15,599 --> 00:00:22,768
communities across our state this is an

10
00:00:19,160 --> 00:00:27,179
historic occasion one that is well

11
00:00:22,768 --> 00:00:31,469
overdue and one that confirms beyond any

12
00:00:27,179 --> 00:00:33,738
doubt I think that in Victoria more than

13
00:00:31,469 --> 00:00:39,679
anywhere else in our nation we

14
00:00:33,738 --> 00:00:44,009
understand we embrace we own and we are

15
00:00:39,679 --> 00:00:46,619
committed to making real that central

16
00:00:44,009 --> 00:00:49,467
truth that better outcomes for

17
00:00:46,619 --> 00:00:52,109
Aboriginal people must be led by

18
00:00:49,469 --> 00:00:54,510
Aboriginal people those words are

19
00:00:52,109 --> 00:00:59,668
uttered often and in many different

20
00:00:54,509 --> 00:01:02,759
forms but here in Victoria all of us can

21
00:00:59,670 --> 00:01:05,729
be abundantly proud but that's not just

22
00:01:02,759 --> 00:01:08,129
a value statement we are living that and

23
00:01:05,728 --> 00:01:10,109
making that real and that is something

24
00:01:08,129 --> 00:01:12,867
that is a great source of pride for

25
00:01:10,109 --> 00:01:16,408
myself for Gavin for Natalie for every

26
00:01:12,868 --> 00:01:19,079
member of our team I want to acknowledge

27
00:01:16,409 --> 00:01:20,909
Jill Gallagher and her work I've known

28
00:01:19,079 --> 00:01:24,359
Jill for a long time and I credit Jill

29
00:01:20,909 --> 00:01:27,450
with my appreciation of that central

30
00:01:24,359 --> 00:01:29,008
truth we first worked together when I

31
00:01:27,450 --> 00:01:32,039
was Health Minister and she was of

32
00:01:29,009 --> 00:01:36,289
course at botch oh and she taught me in

33
00:01:32,040 --> 00:01:39,780
very practical terms that we can't have

34
00:01:36,290 --> 00:01:42,719
we can't have decisions being made that

35
00:01:39,780 --> 00:01:44,728
directly impact Aboriginal families and

36
00:01:42,719 --> 00:01:48,598
communities without a strong Aboriginal

37
00:01:44,728 --> 00:01:52,019
voice in not just the process but in the

38
00:01:48,599 --> 00:01:54,297
active decision-making that is true in

39
00:01:52,019 --> 00:01:57,749
health it's true in so many different

40
00:01:54,299 --> 00:01:59,850
sectors and it's true when it comes to

41
00:01:57,750 --> 00:02:04,559
the notion of making self-determination

42
00:01:59,849 --> 00:02:08,127
real not just fine words but indeed a

43
00:02:04,560 --> 00:02:10,619
process that will not be easy it will be

44
00:02:08,128 --> 00:02:12,559
challenging it will test us it will push

45
00:02:10,618 --> 00:02:14,978
us it will

46
00:02:12,560 --> 00:02:17,479
time to time be very difficult I expect

47
00:02:14,979 --> 00:02:19,759
but we're not here because this is an

48
00:02:17,479 --> 00:02:22,129
easy process if it were easy it would

49
00:02:19,759 --> 00:02:25,669
perhaps have been delivered many many

50
00:02:22,129 --> 00:02:28,909
years ago it wasn't but are now forced

51
00:02:25,669 --> 00:02:32,329
to us this unique opportunity to do more

52
00:02:28,909 --> 00:02:34,279
to do better to deal with all of those

53
00:02:32,330 --> 00:02:37,730
challenges that will come along the way

54
00:02:34,280 --> 00:02:41,869
and if I might be permitted to make a

55
00:02:37,729 --> 00:02:43,969
fairly parochial point as a state we are

56
00:02:41,870 --> 00:02:47,509
I think at our best when we lead our

57
00:02:43,969 --> 00:02:51,259
nation and for some unknown reason we as

58
00:02:47,509 --> 00:02:54,968
a nation find it difficult to do

59
00:02:51,259 --> 00:02:58,039
meaningful things with that wonderful

60
00:02:54,969 --> 00:03:00,768
truth-telling poetry that is that all

61
00:02:58,039 --> 00:03:03,949
the rest even we find it as a nation

62
00:03:00,769 --> 00:03:07,129
impossible to do what all of you have

63
00:03:03,949 --> 00:03:09,798
done come here with different

64
00:03:07,129 --> 00:03:13,129
perspectives with different voices with

65
00:03:09,799 --> 00:03:15,829
different lived experience but United in

66
00:03:13,129 --> 00:03:19,069
a common purpose that we have to

67
00:03:15,830 --> 00:03:22,280
together do everything we can to heal

68
00:03:19,069 --> 00:03:26,418
the wounds of the past and we have to

69
00:03:22,280 --> 00:03:29,060
build a shared common future one that is

70
00:03:26,419 --> 00:03:32,207
hopeful one that is realistic at the

71
00:03:29,060 --> 00:03:35,239
same time but one that is truly about

72
00:03:32,209 --> 00:03:37,1000
self-determination the alternative is to

73
00:03:35,239 --> 00:03:40,578
skirt around these issues never finding

74
00:03:38,000 --> 00:03:43,068
the courage to lead never finding the

75
00:03:40,579 --> 00:03:46,760
courage to face up to those terrible

76
00:03:43,068 --> 00:03:49,128
injustice 'as of the past now I want to

77
00:03:46,759 --> 00:03:52,159
say a couple things about where we are

78
00:03:49,129 --> 00:03:56,178
today and I know that for many this is a

79
00:03:52,159 --> 00:03:59,599
symbol of injustice this building 163

80
00:03:56,180 --> 00:04:01,208
years of injustice but as Jill I think

81
00:03:59,599 --> 00:04:05,267
very eloquently made the point yesterday

82
00:04:01,209 --> 00:04:08,239
it's fitting that a reset of this

83
00:04:05,269 --> 00:04:12,379
partnership a reset of our relationship

84
00:04:08,239 --> 00:04:14,269
and our way forward should arrive from

85
00:04:12,379 --> 00:04:18,499
this place should should come from this

86
00:04:14,269 --> 00:04:19,788
place and I am determined as every

87
00:04:18,500 --> 00:04:22,490
member of the government and through the

88
00:04:19,788 --> 00:04:25,818
government every Victorian to a much

89
00:04:22,490 --> 00:04:26,509
more honest and much more just a much

90
00:04:25,819 --> 00:04:31,878
more include

91
00:04:26,509 --> 00:04:34,749
and respectful future and as difficult

92
00:04:31,879 --> 00:04:37,279
as it is to perhaps be in this building

93
00:04:34,750 --> 00:04:40,160
we will look back I think he needs to

94
00:04:37,279 --> 00:04:42,559
come and be very proud of what has been

95
00:04:40,160 --> 00:04:45,469
achieved to this point and all that will

96
00:04:42,560 --> 00:04:47,029
come thereafter to each of you can I say

97
00:04:45,470 --> 00:04:49,249
thank you for your leadership in

98
00:04:47,029 --> 00:04:53,238
individual communities and I thank you

99
00:04:49,250 --> 00:04:56,360
those who went through the democratic

100
00:04:53,240 --> 00:04:59,509
election process can I thank you it's

101
00:04:56,360 --> 00:05:01,879
not easy to run for public office many

102
00:04:59,509 --> 00:05:04,039
of us know that all of us now know that

103
00:05:01,879 --> 00:05:06,199
it can be very challenging there are

104
00:05:04,040 --> 00:05:08,929
many different views there are many

105
00:05:06,199 --> 00:05:11,269
different opinions but it's only by

106
00:05:08,930 --> 00:05:14,660
taking a stand it's only by putting

107
00:05:11,269 --> 00:05:17,559
yourself forward that we can hope to

108
00:05:14,660 --> 00:05:22,429
find that consensus find an agreement

109
00:05:17,560 --> 00:05:25,279
that is about yes the past and terrible

110
00:05:22,430 --> 00:05:28,1000
awful things that dominate our history

111
00:05:25,279 --> 00:05:31,668
but also find that shared connection and

112
00:05:29,000 --> 00:05:32,680
bond and that unified position to take

113
00:05:31,670 --> 00:05:35,600
us forward

114
00:05:32,680 --> 00:05:37,370
now I'm abundantly conscious as well

115
00:05:35,600 --> 00:05:39,200
that this Q&A at the end of this so long

116
00:05:37,370 --> 00:05:41,229
speeches are never good speeches but I

117
00:05:39,199 --> 00:05:43,729
want to get to that as quickly as I can

118
00:05:41,230 --> 00:05:47,510
but I just hope that I leave you with a

119
00:05:43,730 --> 00:05:49,810
clear sense that we have embarked on

120
00:05:47,509 --> 00:05:52,398
this process because each of you and

121
00:05:49,810 --> 00:05:54,350
through you Aboriginal communities

122
00:05:52,399 --> 00:05:57,229
across the state said that no

123
00:05:54,350 --> 00:05:58,850
self-determination conversation would be

124
00:05:57,230 --> 00:06:01,600
complete or it would simply be a

125
00:05:58,850 --> 00:06:04,220
conversation words without action unless

126
00:06:01,600 --> 00:06:06,470
treaty was on the table and unless we

127
00:06:04,220 --> 00:06:09,729
took big and bold steps to deliver

128
00:06:06,470 --> 00:06:14,239
treaty that's what brings us here today

129
00:06:09,730 --> 00:06:16,460
listening to Aboriginal people nothing

130
00:06:14,240 --> 00:06:18,799
about this process will be easy nothing

131
00:06:16,459 --> 00:06:22,069
about this process will be quick but

132
00:06:18,800 --> 00:06:23,780
important work really is I want to thank

133
00:06:22,069 --> 00:06:25,399
you for your leadership I want to thank

134
00:06:23,779 --> 00:06:27,859
you for your participation for your

135
00:06:25,399 --> 00:06:30,049
commitment with this opportunity comes

136
00:06:27,860 --> 00:06:33,319
great responsibility for each of you and

137
00:06:30,050 --> 00:06:38,339
for us and for our part I commit the

138
00:06:33,319 --> 00:06:42,248
government its agencies I commit all of

139
00:06:38,339 --> 00:06:44,619
- doing everything we can to get us to a

140
00:06:42,250 --> 00:06:47,019
place where in decades to come we can

141
00:06:44,620 --> 00:06:49,990
look back with a real sense of pride and

142
00:06:47,019 --> 00:06:52,148
purpose and know that this day was true

143
00:06:49,990 --> 00:06:56,259
national leadership that this day

144
00:06:52,149 --> 00:06:58,748
started a process that got us to a more

145
00:06:56,259 --> 00:07:01,919
decent future a more honest future a

146
00:06:58,750 --> 00:07:06,190
future that's about shared connection a

147
00:07:01,918 --> 00:07:10,838
celebration of our rich history all 40

148
00:07:06,189 --> 00:07:13,679
50 60 thousand years of it while facing

149
00:07:10,839 --> 00:07:16,388
up to the terrible injustice of the past

150
00:07:13,680 --> 00:07:18,250
giving not just a voice but real power

151
00:07:16,389 --> 00:07:20,079
to Aboriginal people and building a

152
00:07:18,250 --> 00:07:22,750
better Victoria that is not just good

153
00:07:20,079 --> 00:07:25,779
for Aboriginal Victorians that is a more

154
00:07:22,750 --> 00:07:29,379
fair just decent and inclusive Victoria

155
00:07:25,779 --> 00:07:31,748
for all Victorians this work is being

156
00:07:29,379 --> 00:07:34,178
led by Aboriginal people but the

157
00:07:31,750 --> 00:07:36,940
beneficiaries are a much broader group

158
00:07:34,180 --> 00:07:39,699
they are in fact every single Victorian

159
00:07:36,939 --> 00:07:42,469
and for that I am proud and very

160
00:07:39,699 --> 00:07:50,788
grateful to you thank you very much

161
00:07:42,470 --> 00:07:50,789
[Applause]

