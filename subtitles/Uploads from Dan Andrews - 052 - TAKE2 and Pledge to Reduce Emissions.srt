1
00:00:00,560 --> 00:00:02,280
The earth is getting warmer.

2
00:00:02,280 --> 00:00:04,600
Our climate is changing rapidly.

3
00:00:04,600 --> 00:00:06,220
To slow down this change,

4
00:00:06,220 --> 00:00:08,460
we all must pledge to TAKE2.

5
00:00:08,470 --> 00:00:14,550
TAKE2 is Victoria's climate change pledge to keep
the temperature rise to under two degrees by 2050.

6
00:00:14,550 --> 00:00:17,000
It's time for us all to play our part

7
00:00:17,000 --> 00:00:19,080
in Victoria's action on climate change.

8
00:00:19,080 --> 00:00:21,820
I will take two minutes to
keep the Dandenongs healthy.

9
00:00:21,820 --> 00:00:24,840
I will take two so the waterfront doesn't rise.

10
00:00:24,840 --> 00:00:27,540
I'll take two for my grandkids' future,

11
00:00:27,540 --> 00:00:29,740
to create new jobs in clean energy,

12
00:00:29,740 --> 00:00:31,100
for small business.

13
00:00:31,100 --> 00:00:34,820
I'll take two to keep our seasonal local produce in the same seasons.

14
00:00:34,820 --> 00:00:36,760
I'll take two for our farmers.

15
00:00:36,760 --> 00:00:38,940
I'll take two to keep football in winter.

16
00:00:38,940 --> 00:00:43,160
I will take two and commit to doing
my part to reduce Victoria's emissions.

17
00:00:43,160 --> 00:00:48,240
To keep the temperature rise to under two
degrees, we must all take action.

18
00:00:48,240 --> 00:00:53,400
Pledge to TAKE2 now at take2.vic.gov.au.

