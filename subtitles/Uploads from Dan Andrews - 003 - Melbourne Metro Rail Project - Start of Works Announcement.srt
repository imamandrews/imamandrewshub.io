1
00:00:04,129 --> 00:00:08,338
today I'm very pleased to make two

2
00:00:06,389 --> 00:00:10,557
significant announcements firstly we

3
00:00:08,339 --> 00:00:12,688
have bought forward 40 million dollars

4
00:00:10,558 --> 00:00:15,538
of that three hundred million dollars in

5
00:00:12,689 --> 00:00:18,419
planning that will be available right

6
00:00:15,539 --> 00:00:20,968
now to make sure that the planning work

7
00:00:18,420 --> 00:00:23,130
is done the balance of the money will be

8
00:00:20,969 --> 00:00:25,379
delivered in the very first budget of

9
00:00:23,129 --> 00:00:27,209
our government as we outlined but the

10
00:00:25,379 --> 00:00:29,367
bring forward of that money means that

11
00:00:27,210 --> 00:00:31,368
planning work doesn't have to wait until

12
00:00:29,368 --> 00:00:34,169
the beginning of the new financial year

13
00:00:31,368 --> 00:00:35,909
the second announcement today beyond the

14
00:00:34,170 --> 00:00:38,939
bring forward of that 40 million dollars

15
00:00:35,909 --> 00:00:41,729
is the Melbourne Metro Rail Authority

16
00:00:38,939 --> 00:00:44,217
this is going to be the dedicated focus

17
00:00:41,729 --> 00:00:47,038
the expertise the hard work that is

18
00:00:44,219 --> 00:00:50,459
necessary to make this very big project

19
00:00:47,039 --> 00:00:52,979
the reality that Victorians want it to

20
00:00:50,460 --> 00:00:57,359
be this is effectively a second city

21
00:00:52,979 --> 00:01:00,927
look this is five brand-new stations

22
00:00:57,359 --> 00:01:02,759
it's 20,000 extra passenger movements in

23
00:01:00,929 --> 00:01:05,280
the morning peak and the afternoon peak

24
00:01:02,759 --> 00:01:07,018
it's a train system and a public

25
00:01:05,280 --> 00:01:09,959
transport system in many respects where

26
00:01:07,019 --> 00:01:11,278
no timetable is needed this is about

27
00:01:09,959 --> 00:01:14,069
making sure that the world's most

28
00:01:11,280 --> 00:01:18,060
livable city has a 21st century public

29
00:01:14,069 --> 00:01:20,698
transport system it is a nine to eleven

30
00:01:18,060 --> 00:01:21,990
billion dollar project that's the

31
00:01:20,700 --> 00:01:24,959
numbers that infrastructure Australia

32
00:01:21,989 --> 00:01:27,029
have put on it it remains infrastructure

33
00:01:24,959 --> 00:01:29,428
Australia's number one infrastructure

34
00:01:27,030 --> 00:01:32,100
priority for our state

35
00:01:29,430 --> 00:01:34,048
it is three and a half thousand jobs it

36
00:01:32,099 --> 00:01:36,318
is a very significant undertaking it's

37
00:01:34,049 --> 00:01:38,520
the biggest investment the biggest

38
00:01:36,319 --> 00:01:41,008
transformation of our public transport

39
00:01:38,519 --> 00:01:44,897
system since the original city loop was

40
00:01:41,009 --> 00:01:44,899
constructed in the early 80s

41
00:01:45,840 --> 00:01:49,710
what we're announcing today is the

42
00:01:47,789 --> 00:01:51,479
bringing forward of 40 million dollars

43
00:01:49,709 --> 00:01:54,058
of our 300 million dollars

44
00:01:51,478 --> 00:01:56,728
commitment to this project to undertake

45
00:01:54,060 --> 00:01:59,008
the detail planning work that is just so

46
00:01:56,728 --> 00:02:01,199
vital to make sure this project is

47
00:01:59,009 --> 00:02:03,630
delivered within the time frames that we

48
00:02:01,200 --> 00:02:06,390
need it to be so we're establishing the

49
00:02:03,629 --> 00:02:08,518
Melbourne Metro authority that authority

50
00:02:06,390 --> 00:02:10,349
will oversee the finalization of the

51
00:02:08,519 --> 00:02:12,900
business case there is work to be done

52
00:02:10,348 --> 00:02:15,359
to refresh that business case and make

53
00:02:12,900 --> 00:02:17,249
sure that alongside of that the detailed

54
00:02:15,360 --> 00:02:19,230
planning work has done the engineering

55
00:02:17,250 --> 00:02:21,660
work has done the site assessments done

56
00:02:19,229 --> 00:02:24,089
and also importantly to the community

57
00:02:21,659 --> 00:02:26,758
consultation is undertaken on the

58
00:02:24,090 --> 00:02:29,160
finalization of the the route and the

59
00:02:26,759 --> 00:02:31,348
design that is a big job and that's why

60
00:02:29,159 --> 00:02:35,987
we don't want to waste one single moment

61
00:02:31,348 --> 00:02:35,987
in getting this project underway

62
00:02:38,469 --> 00:02:40,529
you

